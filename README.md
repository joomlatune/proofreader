# Proofreader #
The Proofreader is a component for Joomla 2.5+ which allows site visitors to report the administrator about found typos on the site.

# Installation #
1. Download the extension to your local machine as a zip file package.
2. From the backend of your Joomla site (administration) select Extensions -> Install/Uninstall.
3. Click the Browse button and select the extension package on your local machine.
4. Click the Upload File & Install button.

# License #
The Proofreader extension is released under [GNU/GPL](http://www.gnu.org/licenses/gpl-3.0.html) license.
