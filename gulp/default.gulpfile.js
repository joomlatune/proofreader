var base64 = require('gulp-base64'),
	bower = require('gulp-bower'),
	concat = require('gulp-concat'),
	flipper = require('gulp-css-flipper'),
	gulp = require('gulp'),
	merge = require('merge-stream'),
	minify = require('gulp-minify-css'),
	replace = require('gulp-replace'),
	sass = require('gulp-ruby-sass'),
	uglify = require('gulp-uglify');

gulp.task('bower', function () {
	return bower()
		.pipe(gulp.dest('./bower_components'));
});

gulp.task('scripts', function () {
	var scripts = gulp.src([
			'./src/js/jquery.noconflict.js',
			'./src/js/jquery.proofreader.js'
		])
			.pipe(gulp.dest('./build/js/')),
		scriptsMin = gulp.src([
			'./src/js/jquery.noconflict.js',
			'./src/js/jquery.proofreader.js'
		])
			.pipe(concat('jquery.proofreader.min.js'))
			.pipe(uglify())
			.pipe(gulp.dest('./build/js/')),
		jqueryMin = gulp.src(['./bower_components/jquery/dist/jquery.min.js'])
			.pipe(gulp.dest('./build/js/'));

	return merge(scripts, scriptsMin, jqueryMin);
});

gulp.task('sass', function () {
	return sass('./src/scss/', {
		'sourcemap=none': true,
		compass         : true
	})
		.on('error', function (err) {
			console.error('Error!', err.message);
		})
		.pipe(gulp.dest('./build/css/'));
});

gulp.task('base64', ['sass'], function () {
	return gulp.src('./build/css/style.css')
		.pipe(base64())
		.pipe(gulp.dest('./build/css/'));
});

gulp.task('rtl', ['base64'], function () {
	return gulp.src('./build/css/style.css')
		.pipe(concat('style_rtl.css'))
		.pipe(flipper())
		.pipe(gulp.dest('./build/css/'));
});

gulp.task('styles', ['rtl'], function () {
	var stylesMin = gulp.src('./build/css/style.css')
			.pipe(concat('style.min.css'))
			.pipe(minify())
			.pipe(gulp.dest('./build/css/')),
		stylesRtlMin = gulp.src('./build/css/style.css')
			.pipe(concat('style_rtl.min.css'))
			.pipe(minify())
			.pipe(gulp.dest('./build/css/')),
		stylesBootstrap = gulp.src('./build/css/style.bootstrap.css')
			.pipe(concat('style.bootstrap.min.css'))
			.pipe(minify())
			.pipe(gulp.dest('./build/css/'));

	return merge(stylesMin, stylesRtlMin, stylesBootstrap);
});

gulp.task('update-copyright', function () {
	var date = new Date(),
		year = date.getFullYear();

	return gulp.src([
		'./src/css/**/*',
		'./src/js/**/*',
		'./src/cms/joomla/**/*',
		'./src/cms/wordpress/**/*',
		'./src/cms/drupal/**/*',
		'./src/views/**/*'
	], {base: '.'})
		.pipe(replace(/\-2014/g, '-' + year))
		.pipe(gulp.dest('./'));
});
