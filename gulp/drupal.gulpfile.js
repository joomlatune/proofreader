var concat = require('gulp-concat'),
	del = require('del'),
	gulp = require('gulp'),
	merge = require('merge-stream'),
	pkg = require('../package.json'),
	replace = require('gulp-replace'),
	zip = require('gulp-zip');

gulp.task('drupal-build-clean', function (cb) {
	del(['./build/drupal/**/*'], cb);
});

gulp.task('drupal-build-clean-css', ['styles'], function (cb) {
	del(['./build/css/drupal/**/*'], cb);
});

gulp.task('drupal-build-styles', ['drupal-build-clean-css'], function () {
	var styles = gulp.src('./build/css/style.css')
			.pipe(concat('proofreader.css'))
			.pipe(gulp.dest('./build/css/drupal/css/')),
		stylesRtl = gulp.src('./build/css/style_rtl.css')
			.pipe(concat('proofreader_rtl.css'))
			.pipe(gulp.dest('./build/css/drupal/css')),
		stylesMin = gulp.src('./build/css/style.css')
			.pipe(concat('proofreader.min.css'))
			.pipe(gulp.dest('./build/css/drupal/css/')),
		stylesRtlMin = gulp.src('./build/css/style.css')
			.pipe(concat('proofreader_rtl.min.css'))
			.pipe(gulp.dest('./build/css/drupal/css'));

	return merge(styles, stylesRtl, stylesMin, stylesRtlMin);
});

gulp.task('drupal-build-copy-files', [
	'scripts',
	'styles',
	'drupal-build-clean',
	'drupal-build-styles'], function () {
	var scripts = gulp.src([
			'./build/js/jquery.proofreader.js',
			'./build/js/jquery.proofreader.min.js'
		])
			.pipe(gulp.dest('./build/drupal/proofreader/js/')),
		styles = gulp.src([
			'./build/css/drupal/css/proofreader.css',
			'./build/css/drupal/css/proofreader.min.css',
			'./build/css/drupal/css/proofreader_rtl.css',
			'./build/css/drupal/css/proofreader_rtl.min.css'
		])
			.pipe(gulp.dest('./build/drupal/proofreader/css/')),
		files = gulp.src([
			'./src/cms/drupal/**/*'
		])
			.pipe(gulp.dest('./build/drupal/proofreader'));

	return merge(scripts, styles, files);
});

gulp.task('drupal-build-set-version', ['drupal-build-copy-files'], function () {
	return gulp.src('./build/drupal/proofreader/proofreader.info')
		.pipe(replace(/(1\.0\.0)/g, pkg.drupal.version))
		.pipe(gulp.dest('./build/drupal/proofreader'));
});

gulp.task('drupal-package', ['drupal-build-set-version'], function () {
	return gulp.src('./build/drupal/**/*', {base: './build/drupal'})
		.pipe(zip('proofreader-7.x-' + pkg.drupal.version + '.zip'))
		.pipe(gulp.dest('./build'));
});