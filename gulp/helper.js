var fs = require('fs');

exports.getFolders = function (folder, recursive) {
	var results = [],
		list = fs.readdirSync(folder);

	list.forEach(function (file) {
		var stat;

		file = folder + '/' + file;
		stat = fs.statSync(file);
		if (stat && stat.isDirectory()) {
			results.push(file);
			if (recursive) {
				results = results.concat(exports.getFolders(file, recursive));
			}
		}
	});
	return results;
};

exports.getFiles = function (folder, recursive, includeFullPath, filter) {
	var results = [],
		list = fs.readdirSync(folder);

	list.forEach(function (file) {
		var filePath = folder + '/' + file,
			stat = fs.statSync(filePath);
		if (stat && stat.isDirectory()) {
			if (recursive) {
				results = results.concat(exports.getFiles(filePath, recursive, includeFullPath, filter));
			}
		} else {
			if (!filter || file.match(filter)) {
				results.push(includeFullPath ? filePath : file);
			}
		}
	});
	return results;
};

exports.getLeadZero = function (value) {
	return (value < 10 ? '0' : '') + value;
};

exports.getFormattedDate = function () {
	var date = new Date(),
		day = date.getDate(),
		month = date.getMonth() + 1,
		year = date.getFullYear();

	return exports.getLeadZero(day) + '/' + exports.getLeadZero(month) + '/' + year;
};

exports.getFormattedTime = function () {
	var date = new Date(),
		hours = date.getHours(),
		minutes = date.getMinutes(),
		seconds = date.getSeconds();

	return exports.getLeadZero(hours) + ':' + exports.getLeadZero(minutes) + ':' + exports.getLeadZero(seconds);
};

exports.createFolder = function (dir) {
	if (!fs.existsSync(dir)) {
		fs.mkdirSync(dir);
	}
};