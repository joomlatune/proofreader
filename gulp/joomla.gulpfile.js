var base64 = require('gulp-base64'),
	concat = require('gulp-concat'),
	del = require('del'),
	file = require('gulp-file'),
	flipper = require('gulp-css-flipper'),
	gulp = require('gulp'),
	helper = require('./helper.js'),
	merge = require('merge-stream'),
	path = require('path'),
	pkg = require('../package.json'),
	replace = require('gulp-replace'),
	zip = require('gulp-zip');

gulp.task('joomla-build-clean', function (cb) {
	del(['./build/joomla/**/*'], cb);
});

gulp.task('joomla-build-clean-css', function (cb) {
	del(['./build/css/joomla/**/*'], cb);
});

gulp.task('joomla-styles-base64-administrator', ['joomla-build-clean-css'], function () {
	return gulp.src('./src/cms/joomla/administrator/assets/css/style.css')
		.pipe(base64())
		.pipe(gulp.dest('./build/css/joomla/administrator/assets/css/'));
});

gulp.task('joomla-styles-rtl-administrator', ['joomla-styles-base64-administrator'], function () {
	return gulp.src('./build/css/joomla/administrator/assets/css/style.css')
		.pipe(concat('style_rtl.css'))
		.pipe(flipper())
		.pipe(gulp.dest('./build/css/joomla/administrator/assets/css/'));
});

gulp.task('joomla-build-copy-files', [
	'scripts',
	'styles',
	'joomla-build-clean',
	'joomla-styles-rtl-administrator'], function () {
	var scripts = gulp.src([
			'./build/js/jquery.noconflict.js',
			'./build/js/jquery.proofreader.js',
			'./build/js/jquery.proofreader.min.js',
			'./build/js/jquery.min.js'
		])
			.pipe(gulp.dest('./build/joomla/media/js/')),
		styles = gulp.src([
			'./build/css/style.css',
			'./build/css/style_rtl.css',
			'./build/css/style.min.css',
			'./build/css/style_rtl.min.css'
		])
			.pipe(gulp.dest('./build/joomla/media/css/')),
		stylesAdministrator = gulp.src([
			'./build/css/joomla/administrator/assets/css/style.css',
			'./build/css/joomla/administrator/assets/css/style_rtl.css'
		])
			.pipe(gulp.dest('./build/joomla/administrator/assets/css/')),
		images = gulp.src([
			'./src/img/proofreader_icon_16x.png',
			'./src/img/proofreader_icon_48x.png'
		])
			.pipe(gulp.dest('./build/joomla/administrator/assets/images/')),
		files = gulp.src([
			'!./src/cms/joomla/administrator/assets/css/style.css',
			'./src/cms/joomla/**/*'
		])
			.pipe(gulp.dest('./build/joomla')),
		donateViews = gulp.src('./src/views/**/*')
			.pipe(gulp.dest('./build/joomla/administrator/layouts/')),
		languages = gulp.src([
			'!./build/languages/transifex/sr@latin',
			'!./build/languages/transifex/sr@latin/*',
			'./build/languages/transifex/**/*'])
			.pipe(gulp.dest('./build/joomla/site/language'));

	return merge(scripts, styles, stylesAdministrator, images, files, donateViews, languages);
});

gulp.task('joomla-build-add-index', ['joomla-build-copy-files'], function () {
	var buildPath = './build/joomla',
		folders = helper.getFolders(buildPath, true),
		tasks;

	folders.push(buildPath);
	tasks = folders.map(function (folder) {
		return gulp.src(path.join(buildPath, folder))
			.pipe(file(path.join('../../', folder, 'index.html'), '<!DOCTYPE html><title></title>'))
			.pipe(gulp.dest(buildPath));
	});

	return merge(tasks);
});

gulp.task('joomla-build-set-version', ['joomla-build-copy-files'], function () {
	var date = helper.getFormattedDate();
	return gulp.src([
		'./build/joomla/proofreader.xml',
		'./build/joomla/plugins/content/proofreader.xml',
		'./build/joomla/plugins/quickicon/proofreader.xml',
		'./build/joomla/plugins/system/proofreader.xml'
	], {base: '.'})
		.pipe(replace(/(<version>)(.*)(<\/version>)/g, '$1' + pkg.joomla.version + '$3'))
		.pipe(replace(/(<creationDate>)(.*)(<\/creationDate>)/g, '$1' + date + '$3'))
		.pipe(gulp.dest('./'));
});

gulp.task('joomla-build-set-site-languages', [
	'joomla-build-copy-files',
	'joomla-build-set-version'], function () {
	var folders = helper.getFolders('./build/joomla/site/language', true),
		languages = '',
		code;

	folders.forEach(function (folder) {
		code = path.basename(folder);
		languages = languages + "\t\t" + '<language tag="' + code + '">' + code + '/' + code + '.com_proofreader.ini</language>' + "\n";
	});

	return gulp.src([
		'./build/joomla/proofreader.xml'
	], {base: '.'})
		.pipe(replace(/(<languages folder="site\/language">[\n\r\t]*)((<language[^>]+>[^<]+<\/language>[\n\r\t]*)+)(<\/languages>)/g, '$1' + "\n" + languages + "\t" + '$4' + "\n"))
		.pipe(gulp.dest('./'));
});

gulp.task('joomla-package', [
	'joomla-build-set-version',
	'joomla-build-set-site-languages',
	'joomla-build-add-index'], function () {
	return gulp.src('./build/joomla/**/*')
		.pipe(zip('com_proofreader_v' + pkg.joomla.version + '.zip'))
		.pipe(gulp.dest('./build'));
});