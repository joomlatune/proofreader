var concat = require('gulp-concat'),
	del = require('del'),
	gulp = require('gulp'),
	merge = require('merge-stream'),
	pkg = require('../package.json'),
	replace = require('gulp-replace'),
	zip = require('gulp-zip');

gulp.task('prestashop-build-clean', function (cb) {
	del(['./build/prestashop/**/*'], cb);
});

gulp.task('prestashop-build-clean-css', ['styles'], function (cb) {
	del(['./build/css/prestashop/**/*'], cb);
});

gulp.task('prestashop-build-copy-files', [
	'scripts',
	'styles',
	'prestashop-build-clean'], function () {
	var scripts = gulp.src([
			'./build/js/jquery.proofreader.js',
			'./build/js/jquery.proofreader.min.js'
		])
			.pipe(gulp.dest('./build/prestashop/proofreader/js/')),
		styles = gulp.src([
			'./build/css/style.css',
			'./build/css/style.min.css',
			'./build/css/style_rtl.css',
			'./build/css/style_rtl.min.css'
		])
			.pipe(gulp.dest('./build/prestashop/proofreader/css/')),
		logoPng = gulp.src('./src/img/proofreader_icon_32x.png')
			.pipe(concat('logo.png'))
			.pipe(gulp.dest('./build/prestashop/proofreader/')),
		logoGif = gulp.src('./src/img/proofreader_icon_16x.gif')
			.pipe(concat('proofreader-adminbar-logo.gif'))
			.pipe(gulp.dest('./build/prestashop/proofreader/img/')),
		files = gulp.src('./src/cms/prestashop/**/*')
			.pipe(gulp.dest('./build/prestashop/proofreader'));

	return merge(scripts, styles, logoPng, logoGif, files);
});

gulp.task('prestashop-build-set-version', ['prestashop-build-copy-files'], function () {
	var config = gulp.src('./build/prestashop/proofreader/config.xml')
			.pipe(replace(/(<!\[CDATA\[1\.0\]\]>)/g, '<![CDATA[' + pkg.prestashop.version + ']]>'))
			.pipe(gulp.dest('./build/prestashop/proofreader')),
		module = gulp.src('./build/prestashop/proofreader/proofreader.php')
			.pipe(replace(/('1.0')/g, "'" + pkg.prestashop.version + "'"))
			.pipe(gulp.dest('./build/prestashop/proofreader'));

	return merge(config, module);
});

gulp.task('prestashop-package', ['prestashop-build-set-version'], function () {
	return gulp.src('./build/prestashop/**/*', {base: './build/prestashop'})
		.pipe(zip('prestashop_proofreader.zip'))
		.pipe(gulp.dest('./build'));
});