var concat = require('gulp-concat'),
	del = require('del'),
	fs = require('fs'),
	gulp = require('gulp'),
	helper = require('./helper.js'),
	request = require('request');

function getLanguageByName(url, auth, name, path) {
	request({
			url    : url + 'translation/' + name + '/',
			headers: {
				'Authorization': auth
			}
		},
		function (error, response, body) {
			var language = JSON.parse(body);
			name = name.replace('_', '-');
			switch (name) {
				case 'eo':
					name = 'eo-XX';
					break;
				case 'sr@latin':
					name = 'sr-YU';
					break;
			}

			helper.createFolder(path);
			helper.createFolder(path + name);
			fs.writeFile(path + name + '/' + name + '.com_proofreader.ini', language.content);
		});
}

gulp.task('transifex-setup', function () {
	var srcFileName = './gulp/transifex.conf.json.example',
		dstFileName = './gulp/transifex.conf.json',
		dstExists = fs.existsSync(dstFileName);


	return gulp.src(dstExists ? [] : [srcFileName])
		.pipe(concat(dstFileName))
		.pipe(gulp.dest('./'));
});

gulp.task('transifex-pull-languages', ['transifex-setup'], function (cb) {
	var path = './build/languages/transifex/',
		conf = require('./transifex.conf.json'),
		auth;

	helper.createFolder(path);

	if (conf) {
		if (conf.url !== '' && conf.username !== '' && conf.password !== '') {
			auth = 'Basic ' + new Buffer(conf.username + ':' + conf.password).toString('base64');
			del([path + '**/*'], cb);

			request({
					url    : conf.url + 'stats/',
					headers: {
						'Authorization': auth
					}
				},
				function (error, response, body) {
					var languages = JSON.parse(body);

					for (var language in languages) {
						if (languages.hasOwnProperty(language)) {
							if (languages[language].completed === '100%') {
								getLanguageByName(conf.url, auth, language, path);
							}
						}
					}
				});
		} else {
			console.log('[' + helper.getFormattedTime() + '] You have to set Transifex.net username and password in /gulp/transifex.conf.json');
		}
	}
});