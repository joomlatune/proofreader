var base64 = require('gulp-base64'),
	concat = require('gulp-concat'),
	del = require('del'),
	flipper = require('gulp-css-flipper'),
	fs = require('fs'),
	gettext = require('gulp-gettext'),
	gulp = require('gulp'),
	helper = require('./helper.js'),
	ini = require('ini'),
	merge = require('merge-stream'),
	minify = require('gulp-minify-css'),
	pkg = require('../package.json'),
	replace = require('gulp-replace'),
	zip = require('gulp-zip');

gulp.task('wordpress-build-clean', function (cb) {
	del(['./build/wordpress/**/*'], cb);
});

gulp.task('wordpress-build-clean-css', ['styles'], function (cb) {
	del(['./build/css/wordpress/**/*'], cb);
});

gulp.task('wordpress-build-styles-base64', ['wordpress-build-clean-css'], function () {
	return gulp.src('./src/cms/wordpress/css/style_admin.css')
		.pipe(base64())
		.pipe(gulp.dest('./build/css/wordpress/css'));
});

gulp.task('wordpress-build-styles', ['wordpress-build-clean-css',
	'wordpress-build-styles-base64'], function () {
	var styles = gulp.src([
			'./build/css/style.css',
			'./src/cms/wordpress/css/style.css'
		])
			.pipe(concat('style.css'))
			.pipe(gulp.dest('./build/css/wordpress/css/')),
		stylesMin = gulp.src([
			'./build/css/style.css',
			'./src/cms/wordpress/css/style.css'
		])
			.pipe(concat('style.min.css'))
			.pipe(minify())
			.pipe(gulp.dest('./build/css/wordpress/css/')),
		stylesRtl = gulp.src([
			'./build/css/style.css',
			'./src/cms/wordpress/css/style.css'
		])
			.pipe(concat('style_rtl.css'))
			.pipe(flipper())
			.pipe(gulp.dest('./build/css/wordpress/css/')),
		stylesRtlMin = gulp.src([
			'./build/css/style.css',
			'./src/cms/wordpress/css/style.css'
		])
			.pipe(concat('style_rtl.min.css'))
			.pipe(flipper())
			.pipe(minify())
			.pipe(gulp.dest('./build/css/wordpress/css')),
		stylesAdministrator = gulp.src('./src/cms/wordpress/css/style_admin.css')
			.pipe(base64())
			.pipe(gulp.dest('./build/css/wordpress/css/')),
		stylesAdministratorRtl = gulp.src('./src/cms/wordpress/css/style_admin.css')
			.pipe(concat('style_admin_rtl.css'))
			.pipe(base64())
			.pipe(flipper())
			.pipe(gulp.dest('./build/css/wordpress/css/'));

	return merge(styles, stylesMin, stylesRtl, stylesRtlMin, stylesAdministrator, stylesAdministratorRtl);
});

gulp.task('wordpress-build-languages-po', ['wordpress-build-clean'], function () {
	var sourcePath = './build/languages/transifex/',
		sourceFile = './build/languages/transifex/en-GB/en-GB.com_proofreader.ini',
		targetPath = './build/languages/wordpress/',
		languages = require('./languages.json'),
		files,
		sourceLanguage,
		replace_joomla_quotes = function (str) {
			return str ? str
				.replace(/^"/g, '')
				.replace(/"$/g, '')
				.replace(/"_QQ_"/g, '\\"') : '';
		};

	helper.createFolder(sourcePath);
	helper.createFolder(targetPath);

	if (!fs.existsSync(sourceFile)) {
		return;
	}

	files = helper.getFiles(sourcePath, true, true, /com_proofreader\.ini/g);
	sourceLanguage = ini.parse(fs.readFileSync(sourceFile, 'utf-8'));

	files.forEach(function (file) {
		if (file !== sourceFile) {
			var translationLanguage = ini.parse(fs.readFileSync(file, 'utf-8')),
				translationFilename = file.split('/').reverse()[0],
				translationLanguageCode = translationFilename.replace('-', '_').split('.')[0],
				content = [];

			content.push('msgid ""');
			content.push('msgstr ""');
			content.push('"Project-Id-Version: ' + pkg.name + ' 1.0\\n"');
			content.push('"Report-Msgid-Bugs-To: https://opentranslators.transifex.com/projects/p/proofreader/\\n"');
			content.push('"Language-Team: OpenTranslators\\n"');
			content.push('"Language: ' + translationLanguageCode + '\\n"');
			content.push('"MIME-Version: 1.0\\n"');
			content.push('"Content-Type: text/plain; charset=UTF-8\\n"');
			content.push('"Content-Transfer-Encoding: 8bit\\n"');

			for (var language in languages) {
				if (languages.hasOwnProperty(language)) {
					if (languages[language].code.replace('-', '_') === translationLanguageCode) {
						content.push('"Plural-Forms: ' + languages[language].plural + '\\n"');
					}
				}
			}

			content.push('');

			for (var prop in sourceLanguage) {
				if (sourceLanguage.hasOwnProperty(prop)) {
					content.push('msgid "' + replace_joomla_quotes(sourceLanguage[prop]) + '"');
					content.push('msgstr "' + replace_joomla_quotes(translationLanguage[prop]) + '"');
					content.push('');
				}
			}
			content = content.join('\n');

			fs.writeFileSync(targetPath + 'proofreader-' + translationLanguageCode + '.po', content);
		}
	});
});

gulp.task('wordpress-build-languages-copy', ['wordpress-build-languages-po'], function () {
	return gulp.src([
		'./build/languages/wordpress/**/*',
		'./src/cms/wordpress/languages/proofreader-admin-ru_RU.po'
	])
		.pipe(gulp.dest('./build/wordpress/languages/'));
});

gulp.task('wordpress-build-languages-compile', ['wordpress-build-languages-copy'], function () {
	return gulp.src('./build/wordpress/languages/**/*.po')
		.pipe(gettext())
		.pipe(gulp.dest('./build/wordpress/languages/'));
});

gulp.task('wordpress-build-copy-files', [
	'wordpress-build-clean',
	'scripts',
	'styles',
	'wordpress-build-languages-compile',
	'wordpress-build-styles'], function () {
	var scripts = gulp.src([
			'./build/js/jquery.noconflict.js',
			'./build/js/jquery.proofreader.js',
			'./build/js/jquery.proofreader.min.js',
			'./build/js/jquery.min.js'
		])
			.pipe(gulp.dest('./build/wordpress/js/')),
		styles = gulp.src([
			'./build/css/wordpress/css/style.css',
			'./build/css/wordpress/css/style_rtl.css',
			'./build/css/wordpress/css/style.min.css',
			'./build/css/wordpress/css/style_rtl.min.css',
			'./build/css/wordpress/css/style_admin.css',
			'./build/css/wordpress/css/style_admin_rtl.css'
		])
			.pipe(gulp.dest('./build/wordpress/css/')),
		images = gulp.src([
			'./src/img/proofreader_icon_16x.png'
		])
			.pipe(gulp.dest('./build/wordpress/img/')),
		donateViews = gulp.src('./src/views/**/*')
			.pipe(gulp.dest('./build/wordpress/views/')),
		files = gulp.src([
			'!./src/cms/wordpress/css/style.css',
			'!./src/cms/wordpress/css/style_admin.css',
			'./src/cms/wordpress/**/*'
		])
			.pipe(gulp.dest('./build/wordpress'));

	return merge(scripts, styles, images, files, donateViews);
});

gulp.task('wordpress-build-set-version', ['wordpress-build-copy-files'], function () {
	var version = pkg.wordpress.version;

	return gulp.src([
		'./build/wordpress/proofreader.php',
		'./build/wordpress/class.proofreader-admin.php'
	])
		.pipe(replace(/(Version: )(.*)/g, '$1' + version))
		.pipe(replace(/(1\.0\.0)/g, version))
		.pipe(gulp.dest('./build/wordpress/'));
});

gulp.task('wordpress-package', ['wordpress-build-set-version'], function () {
	return gulp.src('./build/wordpress/**/*')
		.pipe(zip('proofreader_v' + pkg.wordpress.version + '.zip'))
		.pipe(gulp.dest('./build'));
});