var gulp = require('gulp');
require('require-dir')('./gulp');

gulp.task('setup', ['bower',
                    'transifex-setup']);
gulp.task('default', ['scripts',
                      'styles']);
gulp.task('languages', ['transifex-pull-languages']);
gulp.task('cms', ['joomla-package',
                  'wordpress-package',
                  'drupal-package',
                  'prestashop-package']);
gulp.task('joomla', ['joomla-package']);
gulp.task('wordpress', ['wordpress-package']);
gulp.task('drupal', ['drupal-package']);
gulp.task('prestashop', ['prestashop-package']);

