<?php
/**
 * @file
 * Default theme implementation for the Proofreader.
 *
 * Available variables:
 * - $form: form made from proofreader_form()§
 * - $load_form_url: url for dynamic form load
 *
 * @see template_preprocess_proofreader_container()
 */
?>
<div id="proofreader_container" class="proofreader_container" style="display:none;"><?php echo $form; ?></div>
<script>
	jQuery(document).ready(function ($) {
		$('#proofreader_container').proofreader({
				'handlerType'       : '<?php echo variable_get('proofreader_handler', 'keyboard'); ?>',
				<?php if (isset($load_form_url)) : ?>
				'loadFormUrl'       : '<?php echo $load_form_url; ?>',
				<?php endif; ?>
				<?php if (variable_get('proofreader_highlight_typos')) : ?>
				'highlightTypos'    : true,
				<?php endif; ?>
				'selectionMaxLength': <?php echo (int) variable_get('proofreader_selection_limit', 100); ?>
			},
			{
				'reportTypo'           : '<?php echo t('Report a typo'); ?>',
				'thankYou'             : '<?php echo t('Thank you for reporting the typo!'); ?>',
				'browserIsNotSupported': '<?php echo t('Your browser does not support selection handling.'); ?>',
				'selectionIsTooLarge'  : '<?php echo t('You have selected too large text block!'); ?>'
			});
	});
</script>
