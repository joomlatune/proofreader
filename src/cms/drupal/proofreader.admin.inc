<?php
/**
 * @file
 * Admin settings form for Proofreader module
 */
function proofreader_typos_list()
{
	$form['options'] = array(
		'#type'       => 'fieldset',
		'#title'      => t('Update options'),
		'#attributes' => array('class' => array('container-inline')),
	);

	$form['options']['operation'] = array(
		'#type'          => 'select',
		'#title'         => t('Operation'),
		'#title_display' => 'invisible',
		'#options'       => array('delete' => t('Delete the selected typos')),
		'#default_value' => '',
	);

	$form['options']['submit'] = array(
		'#type'  => 'submit',
		'#value' => t('Update'),
	);

	$header = array(
		'typo'       => array('data' => t('Typo'), 'field' => 'typo_text'),
		'comment'    => array('data' => t('Comment'), 'field' => 'typo_comment'),
		'author'     => array('data' => t('Author'), 'field' => 'created_by_name'),
		'url'        => array('data' => t('URL'), 'field' => 'page_title'),
		'created'    => array('data' => t('Created'), 'field' => 'created', 'sort' => 'desc'),
		'operations' => array('data' => t('Operations')),
	);

	$typos = db_select('proofreader_typos', 'c')
		->extend('PagerDefault')
		->extend('TableSort')
		->fields('c',
			array('id',
				'typo_prefix',
				'typo_text',
				'typo_suffix',
				'typo_comment',
				'page_url',
				'page_title',
				'created_by',
				'created_by_name',
				'created',
			))
		->limit(50)
		->orderByHeader($header)
		->execute();

	$options     = array();
	$destination = drupal_get_destination();

	foreach ($typos as $typo)
	{
		$options[$typo->id] = array(
			'typo'       => check_plain($typo->typo_prefix) . ' <strong>' . check_plain($typo->typo_text) . '</strong> ' . check_plain($typo->typo_suffix),
			'comment'    => check_plain($typo->typo_comment),
			'author'     => check_plain($typo->created_by_name),
			'url'        => array(
				'data' => array(
					'#type'  => 'link',
					'#title' => check_plain($typo->page_title),
					'#href'  => check_url($typo->page_url),
				),
			),
			'created'    => format_date($typo->created, 'short'),
			'operations' => array(
				'data' => array(
					'#type'    => 'link',
					'#title'   => t('delete'),
					'#href'    => '/admin/content/proofreader-typos/' . $typo->id . '/delete',
					'#options' => array('query' => $destination),
				),
			),
		);
	}

	$form['comments'] = array(
		'#type'    => 'tableselect',
		'#header'  => $header,
		'#options' => $options,
		'#empty'   => t('No typos available.'),
	);

	$form['pager'] = array('#theme' => 'pager');

	return $form;
}

/**
 * Deletes typo from list
 *
 * @param $typo_id
 */
function proofreader_typos_delete($typo_id)
{
	$typo_id = (int) $typo_id;

	if ($typo_id)
	{
		db_delete('proofreader_typos')
			->condition('id', $typo_id)
			->execute();

		drupal_set_message(t('Typo deleted!'));
	}

	drupal_goto('admin/content/proofreader-typos');
}

/**
 * Proofreader settings form.
 */
function proofreader_admin_settings()
{
	$form['basic'] = array(
		'#type'  => 'fieldset',
		'#title' => t('Basic'),
	);

	$form['basic']['proofreader_highlight_typos'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Highlight Typos'),
		'#description'   => t('Toggle whether typos should be highlighted on the site.'),
		'#default_value' => (int) variable_get('proofreader_highlight_typos', 1),
		'#options'       => array(
			1 => t('Yes'),
			0 => t('No'),
		),
	);

	$form['basic']['proofreader_notifications'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Notifications'),
		'#description'   => t('Toggle whether email notifications about typos should be sent to the editor.'),
		'#default_value' => (int) variable_get('proofreader_notifications', 0),
		'#options'       => array(
			1 => t('Yes'),
			0 => t('No'),
		),
	);

	$users = proofreader_get_users();

	$form['basic']['proofreader_editor'] = array(
		'#type'          => 'select',
		'#title'         => t('Editor'),
		'#description'   => t('The user who will receive reports about typos on the site.'),
		'#default_value' => (int) variable_get('proofreader_editor', 0),
		'#options'       => $users,
	);

	$form['basic']['proofreader_handler'] = array(
		'#type'          => 'radios',
		'#title'         => t('Handler'),
		'#description'   => t('Select the handler which will be used by the Proofreader for report form. If selected \'Keyboard\' the Proofreader form could be opened by pressing keyboard shortcut Ctrl+Enter, if selected \'Mouse\' the floating button will appear after user has selected something on the page. Or you can use \'Both\' to support both handlers.'),
		'#default_value' => variable_get('proofreader_handler', 'keyboard'),
		'#options'       => array(
			'keyboard' => t('Keyboard (Ctrl+Enter)'),
			'mouse'    => t('Mouse'),
			'both'     => t('Both'),
		),
	);

	$form['form'] = array(
		'#type'  => 'fieldset',
		'#title' => t('Form'),
	);

	$form['form']['proofreader_comment'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Comment'),
		'#description'   => t('You can set whether the user should specify the comment to his report about the found typo.'),
		'#default_value' => (int) variable_get('proofreader_comment', 0),
		'#options'       => array(
			1 => t('Yes'),
			0 => t('No'),
		),
	);

	$form['form']['proofreader_dynamic_form_load'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Dynamic Form Load'),
		'#description'   => t('If set to \'Yes\' the Proofreader\'s form will be loaded dynamically using AJAX. Otherwise it will be included into the page\'s source.'),
		'#default_value' => (int) variable_get('proofreader_dynamic_form_load', 0),
		'#options'       => array(
			1 => t('Yes'),
			0 => t('No'),
		),
	);

	$form['form']['proofreader_captcha'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('CAPTCHA'),
		'#description'   => t('Use reCAPTCHA to protect the Proofreader\'s form from spam bots. IMPORTANT: You MUST provide an reCAPTCHA API keys if you turn this on.'),
		'#default_value' => (int) variable_get('proofreader_captcha', 0),
		'#options'       => array(
			1 => t('Yes'),
			0 => t('No'),
		),
	);

	$form['recaptcha'] = array(
		'#type'  => 'fieldset',
		'#title' => t('reCAPTCHA Keys'),
	);

	$form['recaptcha']['proofreader_recaptcha_site_key'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Site key'),
		'#description'   => t('Used for displaying the CAPTCHA. Grab it <a href="https://www.google.com/recaptcha/admin" target="_blank">Here</a>.'),
		'#default_value' => variable_get('proofreader_recaptcha_site_key', ''),
	);

	$form['recaptcha']['proofreader_recaptcha_secret_key'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Secret key'),
		'#description'   => t('Used for communication between your site and Google. Grab it <a href="https://www.google.com/recaptcha/admin" target="_blank">Here</a>.'),
		'#default_value' => variable_get('proofreader_recaptcha_secret_key', ''),
	);

	$form['advanced'] = array(
		'#type'  => 'fieldset',
		'#title' => t('Advanced'),
	);

	$form['advanced']['proofreader_disable_css'] = array(
		'#type'          => 'checkbox',
		'#title'         => t('Disable CSS'),
		'#description'   => t('Disallow usage of built-in CSS files and thus lower the number of used CSS files. Use this feature if you have already included the Proofreader\'s styles in the main template\'s CSS.'),
		'#default_value' => (int) variable_get('proofreader_disable_css', 0),
		'#options'       => array(
			1 => t('Yes'),
			0 => t('No'),
		),
	);

	$form['advanced']['proofreader_selection_limit'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Selection Limit'),
		'#description'   => t('Sets the maximum text length (number of characters) to be marked as typo.'),
		'#default_value' => (int) variable_get('proofreader_selection_limit', 100),
	);

	return system_settings_form($form);
}

/**
 * Returns list of users which have role 'administrator'
 *
 * @return array Array of users
 */
function proofreader_get_users()
{
	$role   = user_role_load_by_name('administrator');
	$result = db_select('users_roles', 'ur')
		->fields('ur')
		->condition('ur.rid', $role->rid, '=')
		->execute();

	$ids = array();
	foreach ($result as $record)
	{
		$ids[] = $record->uid;
	}

	$users = array(0 => t('Select user'));
	if (count($ids))
	{
		$query  = new EntityFieldQuery();
		$result = $query
			->entityCondition('entity_type', 'user', '=')
			->propertyCondition('uid', $ids, 'IN')
			->propertyCondition('status', 1)
			->execute();

		if (!empty($result['user']))
		{
			$nodes = entity_load('user', array_keys($result['user']));
			foreach ($nodes as $node)
			{
				$users[$node->uid] = check_plain($node->name) . ' (' . check_plain($node->mail) . ')';
			}
		}
	}

	return $users;
}

/**
 * Proofreader settings form validator.
 */
function proofreader_admin_settings_validate($form, &$form_state)
{
	$options = array(
		'proofreader_highlight_typos'      => '1',
		'proofreader_notifications'        => '0',
		'proofreader_handler'              => 'keyboard',
		'proofreader_editor'               => null,
		'proofreader_comment'              => '1',
		'proofreader_selection_limit'      => '100',
		'proofreader_dynamic_form_load'    => '0',
		'proofreader_disable_css'          => '0',
		'proofreader_captcha'              => '0',
		'proofreader_recaptcha_site_key'   => '',
		'proofreader_recaptcha_secret_key' => '',
	);

	foreach ($options as $option_name => $default_value)
	{
		if (isset($form_state['values'][$option_name]))
		{
			$value = $form_state['values'][$option_name];
			switch ($option_name)
			{
				case 'proofreader_handler' :
					$safe_value = in_array($value, ['keyboard', 'mouse', 'both']) ? $value : $default_value;
					break;
				case 'proofreader_selection_limit' :
					$safe_value = max((int) $value, 10);
					break;
				case 'proofreader_recaptcha_site_key' :
				case 'proofreader_recaptcha_secret_key' :
					$safe_value = (string) trim($value);
					break;
				default :
					$safe_value = (int) $value;
			}
		}
		else
		{
			$safe_value = $default_value;
		}

		$form_state['values'][$option_name] = $safe_value;
	}
}