<?php

class AdminProofreaderSettingsController extends ModuleAdminController
{

	public function __construct()
	{

		$context = Context::getContext();

		$tokenModules = Tools::getAdminToken('AdminModules' . (int) (Tab::getIdFromClassName('AdminModules')) . (int) $context->employee->id);
		$url          = 'index.php?controller=AdminModules&configure=proofreader&module_name=proofreader&token=' . $tokenModules;

		header("Location:" . $url);

		parent::__construct();
	}

}