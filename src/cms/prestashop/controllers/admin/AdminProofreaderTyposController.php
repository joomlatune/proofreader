<?php

require_once(_PS_MODULE_DIR_ . 'proofreader/models/ProofreaderTypo.php');

class AdminProofreaderTyposController extends ModuleAdminController
{
	public $bootstrap = true;

	public function __construct()
	{
		$this->module           = 'proofreader';
		$this->table            = 'proofreader_typos';
		$this->className        = 'ProofreaderTypo';
		$this->list_no_link     = true;
		$this->delete           = true;
		$this->edit             = false;
		$this->view             = false;
		$this->allow_export     = false;
		$this->identifier       = 'typo_id';
		$this->_orderBy         = 'created';
		$this->_orderWay        = 'DESC';
		$this->tabAccess['add'] = false;

		$this->addRowAction('delete');
		$this->bulk_actions = array(
			'delete' => array(
				'text'    => $this->l('Delete selected'),
				'confirm' => $this->l('Delete selected items?')
			)
		);

		$this->fields_list = array(
			'typo_id'         => array('title' => $this->l('ID'), 'align' => 'center', 'search' => false),
			'typo_text'       => array('title' => $this->l('Typo'), 'filter_key' => 'a!typo_text', 'callback' => 'displayTypoText',),
			'typo_comment'    => array('title' => $this->l('Comment'), 'filter_key' => 'a!typo_comment'),
			'page_title'      => array('title' => $this->l('Page'), 'filter_key' => 'a!page_title', 'callback' => 'displayPageTitle',),
			'created_by_name' => array('title' => $this->l('Author'), 'filter_key' => 'a!created_by_name'),
			'created'         => array('title' => $this->l('Created'), 'type' => 'date', 'align' => 'center', 'search' => false)
		);

		parent::__construct();

	}

	/**
	 * Callback function for displaying Typo columns
	 *
	 * @param $echo
	 * @param $tr
	 *
	 * @return string
	 */
	public function displayTypoText($echo, $tr)
	{
		return $tr['typo_prefix'] . '<span class="highlight_typo">' . $tr['typo_text'] . '</span>' . $tr['typo_suffix'];
	}

	/**
	 * Callback function for displaying Page columns
	 *
	 * @param $echo
	 * @param $tr
	 *
	 * @return string
	 */
	public function displayPageTitle($echo, $tr)
	{
		$title = htmlspecialchars($tr['page_title']);

		return '<a href="' . $tr['page_url'] . '" title="' . $title . '" target="_blank">' . $title . '</a>';
	}

	public function initToolbar()
	{
		parent::initToolbar();
		unset($this->toolbar_btn['new']);
	}
}