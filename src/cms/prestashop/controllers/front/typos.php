<?php

class ProofreaderTyposModuleFrontController extends AdminController
{
	public $ssl = true;

	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
		include_once($this->module->getLocalPath() . 'models/ProofreaderTypo.php');
	}

	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();
		$action = Tools::getValue('action');

		if (!empty($action) && method_exists($this, 'ajaxProcess' . Tools::toCamelCase($action)))
			$this->{'ajaxProcess' . Tools::toCamelCase($action)}();
		else
			die(Tools::jsonEncode(array('error' => 'method doesn\'t exist')));
	}


	public function ajaxProcessGetForm()
	{
	}

	public function ajaxProcessSubmitTypos()
	{
	}
}
