<?php

class ProofreaderTypo extends ObjectModel
{

	public $typo_text;
	public $typo_prefix;
	public $typo_suffix;
	public $typo_comment;
	public $page_url;
	public $page_title;
	public $page_language;
	public $created;
	public $created_by;
	public $created_by_ip;
	public $created_by_name;

	public static $definition = array(
		'table'     => 'proofreader_typos',
		'primary'   => 'typo_id',
		'multilang' => true,
		'multishop' => true,
		'fields'    => array(
			'typo_text'       => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 3999999999999, 'required' => true),
			'typo_prefix'     => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 3999999999999, 'required' => true),
			'typo_suffix'     => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 3999999999999, 'required' => true),
			'typo_comment'    => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 3999999999999, 'required' => true),
			'page_url'        => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255, 'required' => true),
			'page_title'      => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255, 'required' => true),
			'page_language'   => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 255, 'required' => true),
			'created'         => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
			'created_by'      => array('type' => self::TYPE_INT, 'validate' => 'isInt'),
			'created_by_ip'   => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 39),
			'created_by_name' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'size' => 255),
		)
	);
}

?>
