<?php
if (!defined('_PS_VERSION_'))
	exit;

class Proofreader extends Module
{
	private static $default_configuration_values = array(
		'PROOFREADER_HIGHLIGHT_TYPOS'      => true,
		'PROOFREADER_NOTIFICATIONS'        => false,
		'PROOFREADER_EDITOR'               => '',
		'PROOFREADER_HANDLER'              => 'keyboard',
		'PROOFREADER_COMMENT'              => true,
		'PROOFREADER_DYNAMIC_FORM_LOAD'    => false,
		'PROOFREADER_CAPTCHA'              => false,
		'PROOFREADER_RECAPTCHA_SITE_KEY'   => '',
		'PROOFREADER_RECAPTCHA_SECRET_KEY' => '',
		'PROOFREADER_SELECTION_LIMIT'      => 100,
		'PROOFREADER_DISABLE_CSS'          => false
	);

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->name                   = 'proofreader';
		$this->tab                    = 'others';
		$this->version                = '1.0';
		$this->author                 = 'Joomlatune.com';
		$this->need_instance          = 1;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap              = true;

		parent::__construct();

		$this->displayName      = $this->l('Proofreader');
		$this->description      = $this->l('The Proofreader extension for PrestaShop allows site visitors to report the administrator about found typos on the site.');
		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	}

	public function getContent()
	{
		$output = null;

		if (Tools::isSubmit('submit' . $this->name))
		{
			foreach (self::$default_configuration_values as $key => $value)
			{
				Configuration::updateValue($key, Tools::getValue(strtolower($key)), false, 0, 0);
			}

			$output .= $this->displayConfirmation($this->l('Settings updated'));
		}

		return $output . $this->displayForm();
	}

	public function displayForm()
	{
		$fields_form[0]['form'] = array(
			'legend' => array(
				'title' => $this->l('Settings'),
			),
			'input'  => array(
				array(
					'type'    => 'radio',
					'name'    => 'proofreader_highlight_typos',
					'label'   => $this->l('Highlight Typos'),
					'desc'    => $this->l('Toggle whether typos should be highlighted on the site.'),
					'is_bool' => true,
					'values'  => array(
						array(
							'id'    => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id'    => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
				array(
					'type'    => 'radio',
					'name'    => 'proofreader_notifications',
					'label'   => $this->l('Notifications'),
					'desc'    => $this->l('Toggle whether email notifications about typos should be sent to the editor.'),
					'is_bool' => true,
					'values'  => array(
						array(
							'id'    => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id'    => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
				array(
					'type'   => 'radio',
					'name'   => 'proofreader_handler',
					'label'  => $this->l('Handler'),
					'desc'   => $this->l('Select the handler which will be used by the Proofreader for report form. If selected \'Keyboard\' the Proofreader form could be opened by pressing keyboard shortcut Ctrl+Enter, if selected \'Mouse\' the floating button will appear after user has selected something on the page. Or you can use \'Both\' to support both handlers.'),
					'values' => array(
						array(
							'id'    => 'active_on',
							'value' => 'keyboard',
							'label' => $this->l('Keyboard (Ctrl+Enter)')
						),
						array(
							'id'    => 'active_off',
							'value' => 'mouse',
							'label' => $this->l('Mouse')
						),
						array(
							'id'    => 'active_off',
							'value' => 'both',
							'label' => $this->l('Both')
						),
					),
				),
				array(
					'type'    => 'select',
					'name'    => 'proofreader_editor',
					'label'   => $this->l('Editor'),
					'desc'    => $this->l('The user who will receive reports about typos on the site.'),
					'options' => array(
						'query' => array(
							array(
								'id_option' => 'user_id',
								'name'      => 'user_name'
							),
						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),
				array(
					'type'    => 'radio',
					'name'    => 'proofreader_comment',
					'label'   => $this->l('Comment'),
					'desc'    => $this->l('You can set whether the user should specify the comment to his report about the found typo.'),
					'is_bool' => true,
					'values'  => array(
						array(
							'id'    => 'active_on',
							'value' => 1,
							'label' => $this->l('Show')
						),
						array(
							'id'    => 'active_off',
							'value' => 0,
							'label' => $this->l('Hide')
						)
					),
				),
				array(
					'type'    => 'radio',
					'name'    => 'proofreader_dynamic_form_load',
					'label'   => $this->l('Dynamic Form Load'),
					'desc'    => $this->l('If set to \'Yes\' the Proofreader\'s form will be loaded dynamically using AJAX. Otherwise it will be included into the page\'s source.'),
					'is_bool' => true,
					'values'  => array(
						array(
							'id'    => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id'    => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
				array(
					'type'    => 'radio',
					'name'    => 'proofreader_captcha',
					'label'   => $this->l('CAPTCHA'),
					'desc'    => $this->l('Use reCAPTCHA to protect the Proofreader\'s form from spam bots. IMPORTANT: You MUST provide an reCAPTCHA API keys if you turn this on.'),
					'is_bool' => true,
					'values'  => array(
						array(
							'id'    => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id'    => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
				array(
					'type'  => 'text',
					'label' => $this->l('reCAPTCHA Site key'),
					'desc'  => $this->l('Used for displaying the CAPTCHA. Grab it ') . '<a href="https://www.google.com/recaptcha/admin" target="_blank">' . $this->l('Here') . '</a>.',
					'name'  => 'proofreader_recaptcha_site_key',
					'size'  => 100,
				),
				array(
					'type'  => 'text',
					'label' => $this->l('reCAPTCHA Secret key'),
					'desc'  => $this->l('Used for communication between your site and Google. Grab it ') . '<a href="https://www.google.com/recaptcha/admin" target="_blank">' . $this->l('Here') . '</a>.',
					'name'  => 'proofreader_recaptcha_secret_key',
					'size'  => 100,
				),
				array(
					'type'    => 'radio',
					'name'    => 'proofreader_disable_css',
					'label'   => $this->l('Disable CSS'),
					'desc'    => $this->l('Disallow usage of built-in CSS files and thus lower the number of used CSS files. Use this feature if you have already included the Proofreader\'s styles in the main template\'s CSS.'),
					'is_bool' => true,
					'values'  => array(
						array(
							'id'    => 'active_on',
							'value' => 1,
							'label' => $this->l('Yes')
						),
						array(
							'id'    => 'active_off',
							'value' => 0,
							'label' => $this->l('No')
						)
					),
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button'
			)
		);

		$default_lang = (int) Configuration::get('PS_LANG_DEFAULT');

		$helper = new HelperForm();

		$helper->module                   = $this;
		$helper->name_controller          = $this->name;
		$helper->token                    = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex             = AdminController::$currentIndex . '&configure=' . $this->name;
		$helper->default_form_language    = $default_lang;
		$helper->allow_employee_form_lang = $default_lang;
		$helper->title                    = $this->displayName;
		$helper->submit_action            = 'submit' . $this->name;

		foreach (self::$default_configuration_values as $key => $value)
		{
			Configuration::updateValue($key, $value);
			$helper->fields_value[strtolower($key)] = Configuration::get($key);
		}

		return $helper->generateForm($fields_form);
	}

	public function hookActionAdminControllerSetMedia($params)
	{
		$this->context->controller->addCSS($this->_path . 'css/proofreader.admin.css');
	}

	public function hookProductFooter($params)
	{
		$dynamicFormLoad = Configuration::get('PROOFREADER_DYNAMIC_FORM_LOAD');

		$this->smarty->assign(
			array(
				'proofreader_handler'         => Configuration::get('PROOFREADER_HANDLER'),
				'proofreader_highlight_typos' => Configuration::get('PROOFREADER_HIGHLIGHT_TYPOS'),
				'proofreader_selection_limit' => Configuration::get('PROOFREADER_SELECTION_LIMIT'),
				'form'                        => $dynamicFormLoad ? '' : $this->renderForm(),
				'load_form_url'               => $dynamicFormLoad ? 'homeurl' : ''
			)
		);

		return $this->display(__FILE__, 'views/proofreader_main.tpl');
	}

	public function renderForm()
	{
		$this->smarty->assign(
			array(
				'proofreader_comment' => Configuration::get('PROOFREADER_COMMENT'),
				'proofreader_captcha' => Configuration::get('PROOFREADER_CAPTCHA'),
				'action'              => $this->context->link->getModuleLink($this->name, 'typos', array(), true),
			)
		);

		return $this->display(__FILE__, 'views/proofreader_form.tpl');
	}

	public function hookHeader($params)
	{
		if (!isset($this->context->controller->php_self) || $this->context->controller->php_self != 'product')
			return;
		$this->context->controller->addCSS($this->_path . 'css/style.min.css', 'all');
		$this->context->controller->addJS($this->_path . 'js/jquery.proofreader.js');
	}


	/**
	 * Insert module into database
	 *
	 * @return bool
	 */
	public function install()
	{
		return (
			parent::install() &&
			$this->createTables() &&
			$this->createConfigurationValues() &&
			$this->createAdminTabs() &&
			$this->registerHook('actionAdminControllerSetMedia') &&
			$this->registerHook('productfooter') &&
			$this->registerHook('header')
		);
	}

	/**
	 * Delete module from datable
	 *
	 * @return bool
	 */
	public function uninstall()
	{
		return (
			$this->deleteConfigurationValues() &&
			$this->deleteTables() &&
			$this->deleteAdminTabs() &&
			parent::uninstall()
		);
	}

	/**
	 * @return bool
	 */
	private function createTables()
	{
		return Db::getInstance()->execute('
			CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'proofreader_typos` (
				`typo_id` INT(11) NOT NULL AUTO_INCREMENT,
				`shop_id` int(10) unsigned NOT NULL,
				`typo_text` TEXT NOT NULL,
				`typo_prefix` TEXT NOT NULL DEFAULT \'\',
				`typo_raw` TEXT NOT NULL DEFAULT \'\',
				`typo_suffix` TEXT NOT NULL DEFAULT \'\',
				`typo_comment` TEXT NOT NULL DEFAULT \'\',
				`page_url` VARCHAR(255) NOT NULL DEFAULT \'\',
				`page_title` VARCHAR(255) NOT NULL DEFAULT \'\',
				`page_language` VARCHAR(255) NOT NULL DEFAULT \'\',
				`created` DATETIME NOT NULL DEFAULT \'0000-00-00 00:00:00\',
				`created_by` INT(11) UNSIGNED NOT NULL DEFAULT \'0\',
				`created_by_ip` VARCHAR(39) NOT NULL DEFAULT \'\',
				`created_by_name` VARCHAR(255) NOT NULL DEFAULT \'\',
				PRIMARY KEY (`typo_id`),
				KEY `idx_created_by`(`created_by`)
			)
			ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;
		');
	}

	/**
	 * @return bool
	 */
	private function createAdminTabs()
	{
		$languages = Language::getLanguages();

		$parent_tab   = new Tab();
		$typos_tab    = new Tab();
		$settings_tab = new Tab();
		$about_tab    = new Tab();

		$parent_tab->class_name = 'AdminProofreaderTypos';
		$parent_tab->module     = $this->name;
		$parent_tab->id_parent  = 0;

		foreach ($languages as $language)
		{
			$parent_tab->name[$language['id_lang']]   = $this->l('Proofreader');
			$typos_tab->name[$language['id_lang']]    = $this->l('Typos');
			$settings_tab->name[$language['id_lang']] = $this->l('Settings');
			$about_tab->name[$language['id_lang']]    = $this->l('About');
		}

		$parent_tab->save();

		$typos_tab->class_name    = 'AdminProofreaderTypos';
		$settings_tab->class_name = 'AdminProofreaderSettings';
		$about_tab->class_name    = 'AdminProofreaderAbout';

		foreach (array($typos_tab, $settings_tab, $about_tab) as $tab)
		{
			/** @var $tab TabCore */
			$tab->module    = $this->name;
			$tab->id_parent = $parent_tab->id;
			$tab->save();
		}

		return true;
	}

	/**
	 * @return bool
	 */
	private function createConfigurationValues()
	{
		$result = true;
		foreach (self::$default_configuration_values as $key => $value)
		{
			$result = $result && Configuration::updateValue($key, $value);
		}

		return $result;
	}

	/**
	 * @return bool
	 */
	private function deleteTables()
	{
		return Db::getInstance()->execute('DROP TABLE IF EXISTS `' . _DB_PREFIX_ . 'proofreader_typos`;');
	}

	/**
	 * @return bool
	 */
	private function deleteAdminTabs()
	{
		$result        = true;
		$parent_tab_id = null;

		foreach (array('AdminProofreaderAbout', 'AdminProofreaderSettings', 'AdminProofreaderTypos',) as $class)
		{
			$tab_id = Tab::getIdFromClassName($class);
			if ($tab_id)
			{
				$tab           = new Tab($tab_id);
				$parent_tab_id = $tab->id_parent;
				$tab->delete();
			}
			else
			{
				$result = false;
				break;
			}
		}

		if ($parent_tab_id)
		{
			$tab = new Tab($parent_tab_id);
			$tab->delete();
		}
		else
		{
			$result = false;
		}

		return $result;
	}

	/**
	 * @return bool
	 */
	private function deleteConfigurationValues()
	{
		$result = true;
		foreach (self::$default_configuration_values as $key => $value)
		{
			$result = $result && Configuration::deleteByName($key);
		}

		return $result;
	}
}
