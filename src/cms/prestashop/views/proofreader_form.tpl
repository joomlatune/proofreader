<form id="proofreader_form" action="{$action}" class="proofreader_form" method="post">
	<h2>{l s='Report a typo' mod='proofreader'}</h2>

	<div id="proofreader_messages_container"></div>
	<div>{l s='Typo' mod='proofreader'}</div>
	<div id="proofreader_typo_container" class="proofreader_typo_container"></div>
	{if $proofreader_comment}
		<div>
			<label for="proofreader_comment">{l s='Comment' mod='proofreader'}</label>
		</div>
		<div>
			<textarea name="proofreader[typo_comment]" id="proofreader_comment" class="proofreader_textarea"
					  cols="50" rows="5"></textarea>
		</div>
	{/if}
	{if $proofreader_captcha}
		<div>
			{$proofreader_captcha}
		</div>
	{/if}
	<div class="proofreader_actions">
		<button type="submit" id="proofreader_submit" class="button button-primary">
			{l s='Submit' mod='proofreader'}
		</button>
	</div>
	<div>
		<input type="hidden" name="proofreader[typo_text]" id="proofreader_typo_text" value=""/>
		<input type="hidden" name="proofreader[typo_prefix]" id="proofreader_typo_prefix" value=""/>
		<input type="hidden" name="proofreader[typo_suffix]" id="proofreader_typo_suffix" value=""/>
		<input type="hidden" name="proofreader[page_url]" id="proofreader_page_url"
			   value="{$smarty.server.REQUEST_URI}"/>
		<input type="hidden" name="proofreader[page_title]" id="proofreader_page_title"
			   value="{$meta_title}"/>
		<input type="hidden" name="proofreader[action]" id="proofreader_action" value="submit"/>
	</div>
</form>
