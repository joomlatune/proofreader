<div id="proofreader_container" class="proofreader_container">{if $form}{$form}{/if}</div>
<script>
	jQuery(document).ready(function ($) {
		$('#proofreader_container').proofreader({
			'handlerType': '{$proofreader_handler}',
			{if $load_form_url }
			'loadFormUrl': '{$load_form_url}',
			{/if}
			{if $proofreader_highlight_typos }
			'highlightTypos': true,
			{/if}
			'selectionMaxLength': {$proofreader_selection_limit}
		},
		{
			'reportTypo': '{l s='Report a typo' mod='proofreader'}',
			'thankYou': '{l s='Thank you for reporting the typo!' mod='proofreader'}',
			'browserIsNotSupported': '{l s='Your browser does not support selection handling.' mod='proofreader'}',
			'selectionIsTooLarge': '{l s='You have selected too large text block!' mod='proofreader'}'
		});
	});
</script>
