<?php
defined('ABSPATH') or die('No script kiddies please!');

class Proofreader_Admin
{
	const NONCE = 'proofreader-update-settings';

	private static $initiated = false;
	private static $redirect_url = null;

	public static function init()
	{
		if (!self::$initiated)
		{
			self::init_hooks();
			load_plugin_textdomain('proofreader-admin', false, dirname(plugin_basename(__FILE__)) . '/languages/');
		}

		if (isset($_POST['action']) && 'save-settings' === $_POST['action'])
		{
			self::save_settings();
		}
	}

	public static function init_hooks()
	{
		self::$initiated = true;

		add_action('admin_menu', array('Proofreader_Admin', 'admin_menu'), 5);
		add_action('admin_enqueue_scripts', array('Proofreader_Admin', 'load_resources'));
		add_action('admin_footer-toplevel_page_proofreader', array('Proofreader_Admin', 'insert_redirect_script'));

		add_filter('plugin_action_links_' . plugin_basename(PROOFREADER_PLUGIN_DIR . 'proofreader.php'),
			array('Proofreader_Admin', 'admin_plugin_settings_link'));

		add_action('load-toplevel_page_proofreader', array('Proofreader_Admin', 'screen_options'));
		add_filter('set-screen-option', array('Proofreader_Admin', 'set_screen_option'), 10, 3);

		add_action('wp_dashboard_setup', array('Proofreader_Admin', 'dashboard_widget'));

		add_action('admin_bar_menu', array('Proofreader_Admin', 'admin_bar_menu'), 100);
		add_action('admin_notices', array('Proofreader_Admin', 'admin_notices'));
	}

	public static function admin_menu()
	{
		if (class_exists('Jetpack'))
		{
			add_action('jetpack_admin_menu', array('Proofreader_Admin', 'load_menu'));
		}
		else
		{
			self::load_menu();
		}
	}

	public static function admin_notices()
	{
		if (get_option('proofreader_captcha'))
		{
			$site_key   = get_option('proofreader_recaptcha_site_key');
			$secret_key = get_option('proofreader_recaptcha_secret_key');
			if (empty($site_key) || empty($secret_key))
			{
				echo '<div class="error"><p><strong>' . __('reCAPTCHA API Keys are missing.', 'proofreader-admin') . '</strong></p></div>';
			}
		}
	}

	/**
	 * Removes link to the Proofreader's settings from Wordpress' settings menu
	 *
	 * @param array $links
	 *
	 * @return array
	 */
	public static function admin_plugin_settings_link($links)
	{
		$settings_link = '<a href="' . esc_url(self::get_page_url('settings')) . '">' . __('Settings') . '</a>';
		array_unshift($links, $settings_link);

		return $links;
	}

	/**
	 * Creates menu for the Proofreader
	 */
	public static function load_menu()
	{
		if (class_exists('Jetpack'))
		{
			add_submenu_page('jetpack',
				__('Proofreader', 'proofreader'),
				__('Proofreader', 'proofreader'),
				'manage_options',
				'proofreader-settings',
				array('Proofreader_Admin', 'display_page'));
		}
		else
		{
			$count = Proofreader_Admin::get_typos_count();

			add_menu_page(__('Proofreader', 'proofreader'),
				__('Proofreader', 'proofreader-admin') . ($count ? ' <span class="awaiting-mod"><span>' . $count . '</span></span>' : ''),
				'edit_posts',
				'proofreader',
				array('Proofreader_Admin', 'display_typos_page'),
				PROOFREADER_PLUGIN_URL . 'img/proofreader_icon_16x.png');

			add_submenu_page('proofreader',
				__('Proofreader Typos', 'proofreader-admin'),
				__('Typos', 'proofreader-admin'),
				'edit_posts',
				'proofreader',
				array('Proofreader_Admin', 'display_typos_page'));

			add_submenu_page('proofreader',
				__('Proofreader Settings', 'proofreader-admin'),
				__('Settings'),
				'manage_options',
				'proofreader-settings',
				array('Proofreader_Admin', 'display_settings_page'));

			add_submenu_page('proofreader',
				__('Proofreader About', 'proofreader-admin'),
				__('About', 'proofreader-admin'),
				'edit_posts',
				'proofreader-about',
				array('Proofreader_Admin', 'display_about_page'));
		}
	}

	/**
	 * Loads stylesheets
	 */
	public static function load_resources()
	{
		global $hook_suffix;

		if ('index.php' === $hook_suffix || false !== strpos($hook_suffix, 'proofreader'))
		{
			$style = is_rtl() ? 'style_admin_rtl.css' : 'style_admin.css';
			wp_enqueue_style('proofreader.css', PROOFREADER_PLUGIN_URL . 'css/' . $style, array(), PROOFREADER_VERSION);
		}
	}

	/**
	 * Adds screen option for Typos list
	 */
	public static function screen_options()
	{
		global $typos_list_table;

		add_screen_option('per_page', array(
			'label'   => __('Typos per page', 'proofreader-admin'),
			'default' => 10,
			'option'  => 'proofreader_typos_per_page'
		));

		$typos_list_table = new Proofreader_List_Table;
	}

	/**
	 * Filter a screen option value before it is set.
	 *
	 * @param  bool|int $status Screen option value. Default false to skip.
	 * @param  string   $option The option name.
	 * @param  int      $value  The number of rows to use.
	 *
	 * @return bool Returns option's value if option is equal to 'proofreader_typos_per_page' or false
	 */
	public static function set_screen_option($status, $option, $value)
	{
		return ('proofreader_typos_per_page' === $option) ? $value : $status;
	}

	/**
	 * Updates action links displayed for plugin.
	 *
	 * @param  array  $links An array of plugin action links.
	 * @param  string $file  Path to the plugin file.
	 *
	 * @return array Returns updated list of action links for current plugin
	 */
	public static function plugin_action_links($links, $file)
	{
		if (plugin_basename(PROOFREADER_PLUGIN_URL . '/proofreader.php') === $file)
		{
			$links[] = '<a href="' . esc_url(self::get_page_url()) . '">' . esc_html__('Settings') . '</a>';
		}

		return $links;
	}

	/**
	 * @param string $page
	 *
	 * @return string
	 */
	public static function get_page_url($page = '')
	{
		$page_slug = 'proofreader' . (empty($page) ? '' : '-' . $page);
		$page_url  = add_query_arg(array('page' => $page_slug), admin_url('admin.php'));

		return $page_url;
	}

	public static function display_page()
	{
		if (isset($_GET['view']))
		{
			switch ($_GET['view'])
			{
				case 'typos':
					self::display_typos_page();
					break;
				case 'about':
					self::display_about_page();
					break;
				default:
					self::display_settings_page();
			}
		}
	}

	/**
	 * Displays Typos page
	 */
	public static function display_typos_page()
	{
		$typos_list_table = new Proofreader_List_Table();
		$typos_list_table->process_bulk_action();
		$typos_list_table->prepare_items();

		echo Proofreader::view('typos', array(
			'list_table' => $typos_list_table,
		));
	}

	/**
	 * Displays About page
	 */
	public static function display_about_page()
	{
		$data             = array();
		$data['version']  = '1.0.0';
		$data['homepage'] = 'ru_RU' === get_locale() ? 'http://www.joomlatune.ru' : 'http://www.joomlatune.com';

		echo Proofreader::view('about', $data);
	}

	/**
	 * Displays Settings page
	 */
	public static function display_settings_page()
	{
		load_textdomain('default', WP_LANG_DIR . '/admin-network-' . get_locale() . '.mo');

		$users = array();

		/** @var WP_User_Query $user_query */
		$user_query = new WP_User_Query(array(
			'orderby' => 'display_name',
			'order'   => 'ASC',
			'fields'  => array('ID', 'user_email', 'display_name')
		));

		foreach ($user_query->get_results() as $user)
		{
			/** @var WP_User $user */
			$users[] = array(
				'id'   => $user->ID,
				'name' => sprintf('%1$s (%2$s)', $user->display_name, $user->user_email)
			);
		}

		echo Proofreader::view('settings', array(
			'users' => $users,
		));
	}

	public static function dashboard_widget()
	{
		wp_add_dashboard_widget('proofreader_dashboard_widget', __('Proofreader', 'proofreader-admin'), array('Proofreader_Admin', 'display_dashboard_widget'));
	}

	public static function display_dashboard_widget()
	{
		/* @global wpdb $wpdb The WordPress database class */
		global $wpdb;

		$latest_typos = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}proofreader_typos` ORDER BY `created` DESC LIMIT 5;", ARRAY_A);

		echo Proofreader::view('dashboard-widget', [
			'latest_typos' => $latest_typos,
		]);
	}

	/**
	 * @param WP_Admin_Bar $wp_admin_bar
	 */
	public static function admin_bar_menu($wp_admin_bar)
	{
		if (current_user_can('edit_posts'))
		{
			$count   = Proofreader_Admin::get_typos_count();
			$tooltip = (1 === $count)
				? __('One typo', 'proofreader-admin')
				: sprintf(_n('%s typo', '%s typos', $count, 'proofreader-admin'), number_format_i18n($count));

			$wp_admin_bar->add_menu(array(
				'id'     => 'proofreader-typos',
				'parent' => null,
				'group'  => null,
				'title'  => '<img
							src="' . PROOFREADER_PLUGIN_URL . 'img/proofreader_icon_16x.png"
							style="vertical-align:middle;margin-right:5px;"
							alt="' . __('Proofreader', 'proofreader-admin') . '"/>' .
					'<span class="ab-label awaiting-mod count-' . $count . '">' . $count . '</span>',
				'href'   => Proofreader_Admin::get_page_url(),
				'meta'   => array('title' => $tooltip)
			));
		}
	}

	/**
	 * Returns count of reported typos
	 *
	 * @return int
	 */
	public static function get_typos_count()
	{
		/* @global wpdb $wpdb The WordPress database class */
		global $wpdb;

		$count = (int) $wpdb->get_var("SELECT COUNT(`id`) FROM `{$wpdb->prefix}proofreader_typos`;");

		return $count;
	}

	/**
	 * Saves the Proofreader's settings
	 *
	 * @return bool
	 */
	public static function save_settings()
	{
		check_admin_referer('proofreader_save_settings_nonce');

		if (!current_user_can('manage_options'))
		{
			return false;
		}

		// array('option_name' => 'default_value')
		$options = array(
			'proofreader_highlight_typos'      => '1',
			'proofreader_notifications'        => '0',
			'proofreader_handler'              => 'keyboard',
			'proofreader_editor'               => null,
			'proofreader_comment'              => '1',
			'proofreader_selection_limit'      => '100',
			'proofreader_dynamic_form_load'    => '0',
			'proofreader_disable_css'          => '0',
			'proofreader_captcha'              => '0',
			'proofreader_recaptcha_site_key'   => '',
			'proofreader_recaptcha_secret_key' => '',
		);

		foreach ($options as $option_name => $default_value)
		{
			if (isset($_POST[$option_name]))
			{
				switch ($option_name)
				{
					case 'proofreader_handler' :
						$safe_value = in_array($_POST[$option_name], ['keyboard', 'mouse', 'both']) ? $_POST[$option_name] : $default_value;
						break;
					case 'proofreader_selection_limit' :
						$safe_value = max((int) $_POST[$option_name], 10);
						break;
					case 'proofreader_recaptcha_site_key' :
					case 'proofreader_recaptcha_secret_key' :
						$safe_value = (string) trim($_POST[$option_name]);
						break;
					default :
						$safe_value = (int) $_POST[$option_name];
				}
			}
			else
			{
				$safe_value = $default_value;
			}

			update_option($option_name, $safe_value);
		}

		return true;
	}

	/**
	 * Sets URL of the page to which we will be redirecting
	 *
	 * @param string $redirect_url
	 */
	public static function set_redirect_url($redirect_url)
	{
		self::$redirect_url = $redirect_url;
	}

	/**
	 * Outputs JavaScript code which redirects to the new location
	 */
	public static function insert_redirect_script()
	{
		if (!empty(self::$redirect_url))
		{
			echo '<script type="text/javascript">window.location = "' . self::$redirect_url . '";</script>';
		}
	}
}