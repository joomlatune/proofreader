<?php
defined('ABSPATH') or die('No script kiddies please!');

class Proofreader_Highlighter
{
	public static function highlight($text, $search, $class = "proofreader_highlight", $element = 'span')
	{
		$parts       = preg_split('/\s/', $search);
		$pattern     = '';
		$replacement = '';

		for ($i = 0, $n = count($parts); $i < $n; $i++)
		{
			$pattern .= '(<[^>]+>)?(\s)?(' . preg_quote($parts[$i], '/') . ')(\s)?';
			$replacement .= sprintf('\\%d\\%d<%s class="%s">\\%d</%s>\\%d',
				$i * 4 + 1,
				$i * 4 + 2,
				$element,
				$class,
				$i * 4 + 3,
				$element,
				$i * 4 + 4
			);
		}

		$result = preg_replace('/' . $pattern . '/ism', $replacement, $text);

		return empty($result) ? $text : $result;
	}
}
