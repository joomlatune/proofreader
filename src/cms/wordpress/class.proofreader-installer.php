<?php
defined('ABSPATH') or die('No script kiddies please!');

class Proofreader_Installer
{
	public static function activate($network_wide)
	{
		/* @global wpdb $wpdb The WordPress database class */
		global $wpdb;

		if (function_exists('current_user_can') && !current_user_can('activate_plugins'))
		{
			return;
		}

		$plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
		check_admin_referer("activate-plugin_{$plugin}");

		if (is_multisite() && $network_wide)
		{
			// Get all blogs in the network and activate plugin on each one
			$blog_ids = $wpdb->get_col("SELECT `blog_id` FROM `{$wpdb->blogs}`;");
			foreach ($blog_ids as $blog_id)
			{
				switch_to_blog($blog_id);
				self::create_table();
				restore_current_blog();
			}
		}
		else
		{
			self::create_table();
		}

		add_option('proofreader_db_version', PROOFREADER_DB_SCHEMA_VERSION);
	}

	public static function deactivate()
	{
		/* @global wpdb $wpdb The WordPress database class */
		global $wpdb;

		if (function_exists('current_user_can') && !current_user_can('activate_plugins'))
		{
			return;
		}

		$plugin = isset($_REQUEST['plugin']) ? $_REQUEST['plugin'] : '';
		check_admin_referer("deactivate-plugin_{$plugin}");

		$wpdb->query("DROP TABLE IF EXISTS `{$wpdb->prefix}proofreader_typos`;");
	}

	public static function uninstall()
	{
		if (!defined('WP_UNINSTALL_PLUGIN'))
		{
			exit();
		}

		if (!current_user_can('activate_plugins'))
		{
			return;
		}

		check_admin_referer('bulk-plugins');
	}

	public static function create_table()
	{
		/* @global wpdb $wpdb The WordPress database class */
		global $wpdb;

		$charset_collate = $wpdb->get_charset_collate();

		$sql = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}proofreader_typos` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `typo_text` TEXT NOT NULL,
                `typo_prefix` TEXT NOT NULL DEFAULT '',
                `typo_raw` TEXT NOT NULL DEFAULT '',
                `typo_suffix` TEXT NOT NULL DEFAULT '',
                `typo_comment` TEXT NOT NULL DEFAULT '',
                `page_url` VARCHAR(255) NOT NULL DEFAULT '',
                `page_title` VARCHAR(255) NOT NULL DEFAULT '',
                `page_language` VARCHAR(255) NOT NULL DEFAULT '',
                `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
                `created_by` INT(11) UNSIGNED NOT NULL DEFAULT '0',
                `created_by_ip` VARCHAR(39) NOT NULL DEFAULT '',
                `created_by_name` VARCHAR(255) NOT NULL DEFAULT '',
                PRIMARY KEY (`id`),
                KEY `idx_created_by`(`created_by`),
                KEY `idx_language`(`page_language`)
                ) $charset_collate;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}


	/**
	 * Creating table whenever a new blog is created
	 *
	 * @param int    $blog_id Blog ID.
	 * @param int    $user_id User ID.
	 * @param string $domain  Site domain.
	 * @param string $path    Site path.
	 * @param int    $site_id Site ID. Only relevant on multi-network installs.
	 * @param array  $meta    Meta data. Used to set initial site options.
	 */
	public static function on_create_blog($blog_id, $user_id, $domain, $path, $site_id, $meta)
	{
		if (is_plugin_active_for_network(basename(dirname(__FILE__)) . '/proofreader.php'))
		{
			switch_to_blog($blog_id);
			self::create_table();
			restore_current_blog();
		}
	}

	/**
	 * Deleting the table whenever a blog is deleted
	 *
	 * @param array $tables
	 *
	 * @return array
	 */
	public static function on_delete_blog($tables)
	{
		/* @global wpdb $wpdb The WordPress database class */
		global $wpdb;

		$tables[] = $wpdb->prefix . 'proofreader_typos';

		return $tables;
	}
}