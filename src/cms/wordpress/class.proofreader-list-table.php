<?php
defined('ABSPATH') or die('No script kiddies please!');

if (!class_exists('WP_List_Table'))
{
	require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

/**
 * Class Proofreader_List_Table
 */
class Proofreader_List_Table extends WP_List_Table
{
	/* @var   wpdb $wpdb The WordPress database class */
	private $wpdb = null;

	/**
	 * Constructor.
	 *
	 * @see WP_List_Table::__construct() for more information on default arguments.
	 */
	function __construct()
	{
		global $wpdb;

		parent::__construct(
			array(
				'singular' => 'typo',
				'plural'   => 'typos',
				'ajax'     => false
			)
		);

		$this->wpdb = $wpdb;
	}

	/**
	 * Fires when the default column output is displayed for a single row.
	 *
	 * @param  array  $item
	 * @param  string $column_name
	 *
	 * @return bool|int|string
	 */
	function column_default($item, $column_name)
	{
		switch ($column_name)
		{
			case 'created' :
				$result = mysql2date(get_option('date_format') . ' ' . get_option('time_format'), $item[$column_name]);
				break;
			case 'created_by_name' :
				$result = empty($item[$column_name]) ? __('Guest', 'proofreader-admin') : $item[$column_name];
				break;
			default:
				$result = $item[$column_name];
		}

		return $result;
	}

	/**
	 * Renders Typo column
	 *
	 * @param  array $item
	 *
	 * @return string
	 */
	function column_typo_text($item)
	{
		$excerpt_mode = 'excerpt' === get_user_option('typos_list_mode');

		return sprintf('%1$s<span class="proofreader_highlight">%2$s</span>%3$s %4$s %5$s',
			($excerpt_mode ? $item['typo_prefix'] : ''),
			$item['typo_text'],
			($excerpt_mode ? $item['typo_suffix'] : ''),
			'<div class="info">'
			. '<p><b>' . __('Comment', 'proofreader-admin') . '</b>: ' . $item['typo_comment'] . '</p>'
			. '<p><b>' . __('Author') . '</b>: ' . $item['created_by_name'] . '</p>'
			. '<p><b>' . __('Date') . '</b>: ' . $this->column_default($item, 'created') . '</p>'
			. '</div>',
			$this->row_actions(
				array(
					'delete' => sprintf('<a href="?page=%s&action=delete&typo_id=%s&_wpnonce=%s" title="%s">%s</a>',
						$_REQUEST['page'],
						$item['id'],
						wp_create_nonce('proofreader_typos_actions_nonce'),
						esc_attr__('Delete this item permanently'),
						__('Delete')
					),
					'view'   => sprintf('<a href="%s" title="%s" target="_blank">%s</a>',
						esc_url($item['page_url']),
						esc_attr(sprintf(__('View &#8220;%s&#8221;'), $item['page_title'])),
						__('View')
					)
				)
			)
		);
	}

	/**
	 * Renders column's checkbox element
	 *
	 * @param  array $item
	 *
	 * @return string
	 */
	function column_cb($item)
	{
		return sprintf('<input type="checkbox" name="%1$s[]" value="%2$s" />', 'typo_id', $item['id']);
	}

	/**
	 * Get a list of columns. The format is:
	 * 'internal-name' => 'Title'
	 *
	 * @return array
	 */
	function get_columns()
	{
		return array(
			'cb'              => '<input type="checkbox" />',
			'typo_text'       => __('Typo', 'proofreader-admin'),
			'typo_comment'    => __('Comment', 'proofreader-admin'),
			'created_by_name' => __('Author'),
			'created'         => __('Date'),
		);
	}

	/**
	 * Get a list of sortable columns. The format is:
	 * 'internal-name' => 'orderby'
	 * or
	 * 'internal-name' => array( 'orderby', true )
	 *
	 * The second format will make the initial sorting order be descending
	 *
	 * @return array
	 */
	function get_sortable_columns()
	{
		return array(
			'typo_text'       => array('typo_text', false),
			'typo_comment'    => array('typo_comment', false),
			'created_by_name' => array('created_by_name', false),
			'created'         => array('created', true),
		);
	}

	/**
	 * Get a list of all, hidden and sortable columns, with filter applied
	 *
	 * @return array
	 */
	function get_hidden_columns()
	{
		return (array) get_user_option('managetoplevel_page_proofreadercolumnshidden');
	}

	/**
	 * Get an associative array ( option_name => option_title ) with the list of bulk actions available on this table.
	 *
	 * @return array
	 */
	function get_bulk_actions()
	{
		return array(
			'delete' => __('Delete')
		);
	}

	/**
	 * This method processes bulk actions
	 */
	function process_bulk_action()
	{
		switch ($this->current_action())
		{
			case 'delete' :
			{
				check_admin_referer('proofreader_typos_actions_nonce');

				$typo_id = isset($_GET['typo_id']) ? (array) $_GET['typo_id'] : '';
				if (!empty($typo_id))
				{
					$typo_ids = array_filter(array_map('absint', $typo_id));
					if (!empty($typo_ids))
					{
						$typo_ids_string = implode("', '", $typo_ids);
						$query           = sprintf("DELETE FROM `{$this->wpdb->prefix}proofreader_typos` WHERE `id` IN ('%s')", $typo_ids_string);
						$this->wpdb->query($query);
					}
				}

				if (!empty($_SERVER['HTTP_REFERER']))
				{
					Proofreader_Admin::set_redirect_url($_SERVER['HTTP_REFERER']);
				}
				break;
			}
		}
	}

	/**
	 * @global string $mode
	 *
	 * @param string  $which
	 */
	protected function pagination($which)
	{
		global $mode;

		parent::pagination($which);
		$this->view_switcher($mode);
	}

	/**
	 * Prepares the list of items for displaying.
	 */
	function prepare_items()
	{
		global $mode;

		$order_by = (isset($_REQUEST['orderby']) && array_key_exists($_REQUEST['orderby'], $this->get_sortable_columns()))
			? $_REQUEST['orderby']
			: 'created';

		$order_dir = (isset($_REQUEST['order']) && ('desc' === strtolower($_REQUEST['order'])))
			? 'ASC'
			: 'DESC';

		$where = (empty($_REQUEST['s']))
			? ''
			: sprintf("WHERE `typo_text` LIKE '%1\$s' OR `typo_comment` LIKE '%1\$s' OR `created_by_name` LIKE '%1\$s'",
				'%' . $this->wpdb->escape_by_ref($this->wpdb->esc_like($_REQUEST['s'])) . '%');

		$per_page     = $this->get_items_per_page('proofreader_typos_per_page', 5);
		$current_page = $this->get_pagenum();

		$query       = sprintf("SELECT COUNT(`id`) FROM `{$this->wpdb->prefix}proofreader_typos` %s", $where);
		$total_items = $this->wpdb->get_var($query);
		$total_pages = ceil($total_items / $per_page);

		$limit_start = $current_page > 0 ? (($current_page - 1) * $per_page) : 0;

		$query = sprintf("SELECT * FROM `{$this->wpdb->prefix}proofreader_typos` %s ORDER BY `%s` %s LIMIT %s, %s;", $where, $order_by, $order_dir, $limit_start, $per_page);

		$this->items           = $this->wpdb->get_results($query, ARRAY_A);
		$this->_column_headers = $this->get_column_info();

		if (empty($_REQUEST['mode']))
		{
			$mode = (get_user_option('typos_list_mode') ? get_user_option('typos_list_mode') : 'excerpt');
		}
		else
		{
			$mode = ('excerpt' === $_REQUEST['mode'] ? 'excerpt' : 'list');
		}

		update_user_option(get_current_user_id(), 'typos_list_mode', $mode);

		$this->set_pagination_args(array(
			'total_items' => $total_items,
			'total_pages' => $total_pages,
			'per_page'    => $per_page
		));
	}
}