<?php
defined('ABSPATH') or die('No script kiddies please!');

class Proofreader_Recaptcha
{
	private static $script = 'https://www.google.com/recaptcha/api.js';
	private static $verify_uri = 'https://www.google.com/recaptcha/api/siteverify';
	private static $site_key;
	private static $secret_key;
	private static $initiated = false;

	public static function initialize()
	{
		self::$site_key   = get_option('proofreader_recaptcha_site_key');
		self::$secret_key = get_option('proofreader_recaptcha_secret_key');
		self::$initiated  = !empty(self::$site_key) && !empty(self::$secret_key);

		return self::$initiated;
	}

	public static function get_initiated()
	{
		return self::$initiated;
	}

	public static function get_script()
	{
		return self::$script . '?pa=' . wp_rand();
	}

	public static function display()
	{
		if (self::$initiated)
		{
			echo '<div class="g-recaptcha" data-sitekey="' . self::$site_key . '"></div>';
		}
	}

	public static function verify()
	{
		if (self::$initiated)
		{
			$response      = isset($_POST['g-recaptcha-response']) ? esc_attr($_POST['g-recaptcha-response']) : '';
			$remote_ip     = $_SERVER["REMOTE_ADDR"];
			$request       = wp_remote_get(self::$verify_uri . '?secret=' . self::$secret_key . '&response=' . $response . '&remoteip=' . $remote_ip);
			$response_body = wp_remote_retrieve_body($request);
			$result        = json_decode($response_body, true);

			return $result['success'];
		}
		else
		{
			return false;
		}
	}
}