<?php
defined('ABSPATH') or die('No script kiddies please!');

class Proofreader_Widget extends WP_Widget
{
	/**
	 * Constructor
	 */
	public function __construct()
	{
		load_plugin_textdomain('proofreader-admin', false, dirname(plugin_basename(__FILE__)) . '/languages/');
		$widget_options = array('description' => __('This widget displays the Proofreader\'s help line.', 'proofreader-admin'));
		parent::__construct('Proofreader', __('Proofreader', 'proofreader'), $widget_options);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args     Display arguments including before_title, after_title, before_widget, and after_widget.
	 * @param array $instance The settings for the particular instance of the widget
	 */
	public function widget($args, $instance)
	{
		echo $args['before_widget'];
		echo '<div class="proofreader_prompt">' . __('Found a typo? Please select it and press Ctrl + Enter.', 'proofreader') . '</div>';
		echo $args['after_widget'];
	}
}