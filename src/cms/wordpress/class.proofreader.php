<?php
defined('ABSPATH') or die('No script kiddies please!');

class Proofreader
{
	private static $initiated = false;

	public static function init()
	{
		if (!self::$initiated)
		{
			self::init_hooks();

			load_plugin_textdomain('proofreader', false, dirname(plugin_basename(__FILE__)) . '/languages/');
		}
	}

	public static function init_hooks()
	{
		self::$initiated = true;

		add_action('wp_footer', array('Proofreader', 'load_proofreader'));
		add_action('wp_enqueue_scripts', array('Proofreader', 'load_resources'));

		add_filter('query_vars', array('Proofreader', 'register_vars'));
		add_action('admin_bar_menu', array('Proofreader_Admin', 'admin_bar_menu'), 100);

		if (get_option('proofreader_highlight_typos'))
		{
			add_filter('the_content', array('Proofreader', 'highlight'));
			add_filter('the_excerpt', array('Proofreader', 'highlight'));
			add_filter('comment_text', array('Proofreader', 'highlight'));
		}

		if (get_option('proofreader_captcha') && !get_option('proofreader_dynamic_form_load') && Proofreader_Recaptcha::initialize())
		{
			wp_enqueue_script('proofreader.recaptcha.js', Proofreader_Recaptcha::get_script());
		}
	}

	/**
	 * widget_init activates the plugin widgets
	 */
	public static function widget_init()
	{
		include_once(PROOFREADER_PLUGIN_DIR . 'class.proofreader-widget.php');
		register_widget('Proofreader_Widget');
	}

	/**
	 * Method for displaying widget(prompt message) by shortcode [proofreader_prompt]
	 *
	 * @return string
	 */
	public static function prompt_shortcode()
	{
		include_once(PROOFREADER_PLUGIN_DIR . 'class.proofreader-widget.php');
		ob_start();
		the_widget('Proofreader_Widget');
		$output = ob_get_clean();

		return $output;
	}

	public static function view($name, array $args = array())
	{
		$args = apply_filters('proofreader_view_arguments', $args, $name);

		foreach ($args as $key => $val)
		{
			$$key = $val;
		}

		ob_start();
		include(PROOFREADER_PLUGIN_DIR . 'views/' . $name . '.php');
		$result = ob_get_contents();
		ob_get_clean();

		return $result;
	}

	public static function load_resources()
	{
		$suffix = (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) ? '' : '.min';

		if (!get_option('proofreader_disable_css'))
		{
			$style = is_rtl() ? 'style_rtl' . $suffix . '.css' : 'style' . $suffix . '.css';
			wp_enqueue_style($style, PROOFREADER_PLUGIN_URL . 'css/' . $style, array(), PROOFREADER_VERSION);
		}

		wp_enqueue_script('jquery.proofreader.js', PROOFREADER_PLUGIN_URL . 'js/jquery.proofreader' . $suffix . '.js', array('jquery'), PROOFREADER_VERSION);
	}

	public static function render_form($url = null, $title = null)
	{
		if (empty($url))
		{
			$url = get_permalink();
		}

		if (empty($title))
		{
			$title = wp_title('&raquo;', false);
		}

		$data = array(
			'url'   => $url,
			'title' => $title,
			'hash'  => md5($url . wp_salt())
		);

		if (get_option('proofreader_captcha'))
		{
			Proofreader_Recaptcha::initialize();
		}

		return Proofreader::view('form', $data);
	}

	public static function load_proofreader()
	{
		$dynamicFormLoad = get_option('proofreader_dynamic_form_load', 0);

		$data = array(
			'form'          => $dynamicFormLoad ? '' : self::render_form(),
			'load_form_url' => $dynamicFormLoad ? home_url('?proofreader[action]=form&_wpnonce=' . wp_create_nonce('proofreader_load_form_url_nonce')) : ''
		);

		echo Proofreader::view('proofreader', $data);
	}

	public static function register_vars()
	{
		$vars[] = 'proofreader';

		return $vars;
	}

	public function process()
	{
		$form = get_query_var('proofreader');
		if (!empty($form) && isset($form['action']))
		{
			switch ($form['action'])
			{
				case 'submit':
					self::submit($form);
					break;
				case 'form':
					self::form();
					break;
				default:
					break;
			}
		}
	}

	private static function form()
	{
		if (!wp_verify_nonce($_GET['_wpnonce'], 'proofreader_load_form_url_nonce'))
		{
			die('Security check');
		}

		$response = self::get_form_response($_GET['page_url'], $_GET['page_title']);

		header('Content-Type: application/json');
		echo json_encode($response);
		exit;
	}

	private static function get_form_response($url = null, $title = null)
	{
		$response          = new StdClass();
		$response->error   = false;
		$response->form    = self::render_form($url, $title);
		$response->scripts = array();
		$response->script  = '';

		if (get_option('proofreader_captcha') && Proofreader_Recaptcha::get_initiated())
		{
			$response->scripts[] = Proofreader_Recaptcha::get_script();
		}

		return $response;
	}

	private static function submit($form)
	{
		/* @global wpdb $wpdb The WordPress database class */
		global $wpdb;

		if (!wp_verify_nonce($_POST['_wpnonce'], 'proofreader_submit_typo_nonce'))
		{
			die('Security check');
		}

		$response        = new StdClass;
		$response->error = false;

		if (get_option('proofreader_captcha') && Proofreader_Recaptcha::initialize())
		{
			$result = Proofreader_Recaptcha::verify();
			if (!$result)
			{
				load_plugin_textdomain('proofreader-admin', false, dirname(plugin_basename(__FILE__)) . '/languages/');

				$response->error    = true;
				$response->messages = array(__('Incorrect CAPTCHA. Try again.', 'proofreader-admin'));
			}
		}

		if (false === $response->error)
		{
			$user = wp_get_current_user();

			$typo                    = array();
			$typo['typo_text']       = isset($form['typo_text']) ? $form['typo_text'] : '';
			$typo['typo_prefix']     = isset($form['typo_prefix']) ? $form['typo_prefix'] : '';
			$typo['typo_suffix']     = isset($form['typo_suffix']) ? $form['typo_suffix'] : '';
			$typo['typo_comment']    = isset($form['typo_comment']) ? $form['typo_comment'] : '';
			$typo['page_url']        = isset($form['page_url']) ? $form['page_url'] : '';
			$typo['page_title']      = isset($form['page_title']) ? $form['page_title'] : '';
			$typo['page_language']   = get_locale();
			$typo['created']         = date('Y-m-d H:i:s');
			$typo['created_by']      = $user->ID;
			$typo['created_by_name'] = $user->display_name;
			$typo['created_by_ip']   = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;

			if (false === $wpdb->insert($wpdb->prefix . 'proofreader_typos', $typo))
			{
				$response->error    = true;
				$response->messages = array($wpdb->last_error);
			}
			else
			{
				$response = self::get_form_response($typo['page_url'], $typo['page_title']);

				if (get_option('notifications'))
				{
					$data = array(
						'typo_text'    => $typo['typo_text'],
						'typo_suffix'  => $typo['typo_suffix'],
						'typo_prefix'  => $typo['typo_prefix'],
						'typo_comment' => $typo['typo_comment'],
						'page_title'   => $typo['page_title'],
						'page_url'     => $typo['page_url']
					);

					$email = get_site_option('admin_email');

					$headers[] = 'From: "Site Admin" <' . $email . '>';
					$headers[] = 'content-type: text/html';

					$subject = sprintf(__('The typo was reported on the site \'%s\''), get_bloginfo('title'));
					$body    = Proofreader::view('notification', $data);

					$result = wp_mail($email, $subject, $body, $headers);
					if (true !== $result)
					{
						$response->error    = true;
						$response->messages = array(__('An error was encountered while sending notification email.', 'proofreader'));
					}
				}
			}
		}

		header('Content-Type: application/json');
		echo json_encode($response);
		exit;
	}

	public static function highlight($text)
	{
		/* @global wpdb $wpdb The WordPress database class */
		global $wpdb;

		include_once(PROOFREADER_PLUGIN_DIR . 'class.proofreader-highlighter.php');

		$page_url = get_permalink();
		$query    = "SELECT `typo_text` FROM `{$wpdb->prefix}proofreader_typos` WHERE `page_url`='%s'";
		$typos    = $wpdb->get_results($wpdb->prepare($query, $page_url), ARRAY_N);

		foreach ($typos as $typo)
		{
			$result = Proofreader_Highlighter::highlight($text, $typo[0]);
			if (!empty($result))
			{
				$text = $result;
			}
		}

		return $text;
	}
}