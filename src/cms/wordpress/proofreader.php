<?php
/**
 * Plugin Name: Proofreader
 * Plugin URI: http://www.joomlatune.com/
 * Description: The Proofreader allows site visitors to report the administrator about found typos on the site.
 * Version: 1.0.0
 * Author: Sergey M. Litvinov
 * Author URI: http://www.joomlatune.com
 * Text Domain: proofreader-admin
 * License: GPL2
 */

/*
Copyright 2013-2015 Sergey M. Litvinov (email: smart@joomlatune.com)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

defined('ABSPATH') or die('No script kiddies please!');

define('PROOFREADER_VERSION', '1.0.0');
define('PROOFREADER_DB_SCHEMA_VERSION', '1.0');
define('PROOFREADER_MINIMUM_WP_VERSION', '4.0');
define('PROOFREADER_PLUGIN_URL', plugin_dir_url(__FILE__));
define('PROOFREADER_PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once(PROOFREADER_PLUGIN_DIR . 'class.proofreader.php');
require_once(PROOFREADER_PLUGIN_DIR . 'class.proofreader-installer.php');
require_once(PROOFREADER_PLUGIN_DIR . 'class.proofreader-admin.php');
require_once(PROOFREADER_PLUGIN_DIR . 'class.proofreader-recaptcha.php');

register_activation_hook(__FILE__, array('Proofreader_Installer', 'activate'));
register_deactivation_hook(__FILE__, array('Proofreader_Installer', 'deactivate'));

add_action('init', array('Proofreader', 'init'));
add_action('widgets_init', array('Proofreader', 'widget_init'));
add_action('template_redirect', array('Proofreader', 'process'));
add_action('wpmu_new_blog', array('Proofreader_Installer', 'on_create_blog'), 10, 6);
add_filter('wpmu_drop_tables', array('Proofreader_Installer', 'on_delete_blog'));
add_shortcode('proofreader_prompt', array('Proofreader', 'prompt_shortcode'));

if (is_admin())
{
	require_once(PROOFREADER_PLUGIN_DIR . 'class.proofreader-list-table.php');
	add_action('init', array('Proofreader_Admin', 'init'));
}
