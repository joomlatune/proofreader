=== Proofreader ===
Contributors: joomlatune
Tags: proofreader, typo, error
Donate link: http://www.joomlatune.com/proofreader.html
Requires at least: 4.0
Tested up to: 4.2.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

The Proofreader allows site visitors to report the administrator about found typos on the site.

== Description ==

The site visitor is able to select found typo by mouse and press the key combination Ctrl+Enter to send the message to site administrator.
After this the popup window will be shown and he will be able to write some comment about found typo.
The report about typo will be available in administrators panel and the e-mail notification will be sent to administrator.

* Uses the AJAX to send the report without reloading the current web page
* Sends e-mail notifications to administrator
* Supports the spam protection by reCAPTCHA
* User can post comment about found typo

== Installation ==

1. Download the plugin to your computer
2. Log in to your WP admin area and go to Plugins > Add New.
3. Browse to the plugin archive and select it.
4. Then click Install Now and the plugin will be installed shortly.
5. Activate the plugin through the "Plugins" menu in WordPress.

== Changelog ==

= 1.0.0 =

* Initial release.

