<?php
defined('ABSPATH') or die('No script kiddies please!');
?>
<div class="wrap">

	<h2><?php esc_html_e('About'); ?></h2>

	<div id="proofreader-about">
		<div>
			<span class="proofreader-name">Proofreader</span>
			<span class="proofreader-version"><?php echo $version; ?></span>
		</div>
		<div class="proofreader-copyright">
			&copy; 2013-<?php echo date('Y'); ?> <a href="<?php echo $homepage; ?>">JoomlaTune Team</a>. <?php echo __('All rights reserved!', 'proofreader-admin'); ?>
		</div>
		<div class="proofreader-description">
			<?php echo __('The Proofreader allows site visitors to report the administrator about found typos on the site.', 'proofreader-admin'); ?>
		</div>
		<div class="proofreader-license">
			<?php echo __('The Proofreader is Free Software and is distributed under the terms of the <a href="http://www.gnu.org/licenses/gpl-2.0.html">GNU General Public License</a>.', 'proofreader-admin'); ?>
		</div>
		<div class="proofreader-donate">
			<?php if ('ru_RU' === get_locale()): ?>
				<?php require_once(dirname(__FILE__) . '/donate.yandex.php'); ?>
			<?php else: ?>
				<?php require_once(dirname(__FILE__) . '/donate.paypal.php'); ?>
			<?php endif; ?>
		</div>
	</div>
</div>