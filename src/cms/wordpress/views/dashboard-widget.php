<?php defined('ABSPATH') or die('No script kiddies please!'); ?>
<div class="activity-block">
	<?php if (count($latest_typos)) : ?>
		<div>
			<?php foreach ($latest_typos as $item): ?>
				<div class="typo-item">
					<h4>
						<span class="date">
							<?php echo sprintf('%s | %s | <a href="%s" title="%s" target="_blank">%s</a>',
								mysql2date(get_option('date_format') . ', ' . get_option('time_format'), $item['created']),
								esc_attr(empty($item['created_by_name']) ? __('Guest', 'proofreader-admin') : $item['created_by_name']),
								esc_url($item['page_url']),
								esc_attr(sprintf(__('View &#8220;%s&#8221;'), $item['page_title'])),
								esc_attr($item['page_title'])
							); ?>
						</span>
					</h4>
					<blockquote>
						<?php echo sprintf('%1$s<span class="proofreader_highlight">%2$s</span>%3$s',
							$item['typo_prefix'],
							$item['typo_text'],
							$item['typo_suffix']
						); ?>
					</blockquote>
					<span class="row-actions">
						<span class="delete">
							<?php echo sprintf('<a href="?page=proofreader&action=delete&typo_id=%s&_wpnonce=%s" title="%s">%s</a>',
								$item['id'],
								wp_create_nonce('proofreader_typos_actions_nonce'),
								esc_attr__('Delete this item permanently'),
								__('Delete')
							); ?>
						</span>
						|
						<span class="view">
							<?php echo sprintf('<a href="%s" title="%s" target="_blank">%s</a>',
								esc_url($item['page_url']),
								esc_attr(sprintf(__('View &#8220;%s&#8221;'), $item['page_title'])),
								__('View')
							); ?>
						</span>
					</span>
				</div>
			<?php endforeach; ?>
		</div>
	<?php else: ?>
		<div class="not-found">
			<?php esc_html_e('There are no reported typos', 'proofreader-admin'); ?>
		</div>
	<?php endif; ?>
	<ul class="subsubsub">
		<li>
			<a href="<?php echo esc_url(Proofreader_Admin::get_page_url()); ?>">
				<?php esc_html_e('Typos', 'proofreader-admin'); ?>
			</a> |
		</li>
		<?php if (current_user_can('manage_settings')) : ?>
			<li>
				<a href="<?php echo esc_url(Proofreader_Admin::get_page_url('settings')); ?>">
					<?php esc_html_e('Settings'); ?>
				</a> |
			</li>
		<?php endif; ?>
		<li>
			<a href="<?php echo esc_url(Proofreader_Admin::get_page_url('about')); ?>">
				<?php esc_html_e('About', 'proofreader-admin'); ?>
			</a>
		</li>
	</ul>
</div>