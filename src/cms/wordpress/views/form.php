<?php
defined('ABSPATH') or die('No script kiddies please!');
?>
<form id="proofreader_form" action="<?php echo get_home_url() . '/'; ?>" class="proofreader_form" method="post">
	<h2><?php echo esc_html__('Report a typo', 'proofreader'); ?></h2>

	<div id="proofreader_messages_container"></div>
	<div><?php echo esc_html__('Typo', 'proofreader'); ?></div>
	<div id="proofreader_typo_container" class="proofreader_typo_container"></div>
	<?php if (get_option('proofreader_comment')) : ?>
		<div>
			<label for="proofreader_comment"><?php echo esc_html__('Comment', 'proofreader'); ?></label>
		</div>
		<div>
			<textarea name="proofreader[typo_comment]" id="proofreader_comment" class="proofreader_comment"
			          cols="50" rows="5"></textarea>
		</div>
	<?php endif; ?>
	<?php if (get_option('proofreader_captcha') && Proofreader_Recaptcha::get_initiated()) : ?>
		<div>
			<?php Proofreader_Recaptcha::display(); ?>
		</div>
	<?php endif; ?>
	<div class="proofreader_actions">
		<button type="submit" id="proofreader_submit" class="button button-primary">
			<?php echo esc_html__('Submit', 'proofreader'); ?>
		</button>
	</div>
	<div>
		<input type="hidden" name="proofreader[typo_text]" id="proofreader_typo_text" value=""/>
		<input type="hidden" name="proofreader[typo_prefix]" id="proofreader_typo_prefix" value=""/>
		<input type="hidden" name="proofreader[typo_suffix]" id="proofreader_typo_suffix" value=""/>
		<input type="hidden" name="proofreader[page_url]" id="proofreader_page_url"
		       value="<?php echo esc_url($url); ?>"/>
		<input type="hidden" name="proofreader[page_title]" id="proofreader_page_title"
		       value="<?php echo esc_attr($title); ?>"/>
		<input type="hidden" name="proofreader[hash]" id="proofreader_hash"
		       value="<?php echo esc_attr($hash); ?>"/>
		<input type="hidden" name="proofreader[action]" id="proofreader_action" value="submit"/>
		<?php wp_nonce_field('proofreader_submit_typo_nonce', '_wpnonce', false); ?>
	</div>
</form>
