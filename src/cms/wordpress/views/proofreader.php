<?php
defined('ABSPATH') or die('No script kiddies please!');
?>
<div id="proofreader_container" class="proofreader_container" style="display:none;"><?php echo empty($form) ? '' : $form; ?></div>
<script>
	jQuery(document).ready(function ($) {
		$('#proofreader_container').proofreader({
				'handlerType'       : '<?php echo get_option('proofreader_handler', 'keyboard'); ?>',
				<?php if (isset($load_form_url)) : ?>
				'loadFormUrl'       : '<?php echo $load_form_url; ?>',
				<?php endif; ?>
				<?php if (get_option('proofreader_highlight_typos')) : ?>
				'highlightTypos'    : true,
				<?php endif; ?>
				'selectionMaxLength': <?php echo (int) get_option('proofreader_selection_limit', 100); ?>
			},
			{
				'reportTypo'           : '<?php echo esc_js(__('Report a typo', 'proofreader')); ?>',
				'thankYou'             : '<?php echo esc_js(__('Thank you for reporting the typo!', 'proofreader')); ?>',
				'browserIsNotSupported': '<?php echo esc_js(__('Your browser does not support selection handling.', 'proofreader')); ?>',
				'selectionIsTooLarge'  : '<?php echo esc_js(__('You have selected too large text block!', 'proofreader')); ?>'
			});
	});
</script>
