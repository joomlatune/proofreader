<?php defined('ABSPATH') or die('No script kiddies please!'); ?>
<div class="wrap">

	<h2><?php esc_html_e('Settings'); ?></h2>

	<form id="proofreader_settings"
	      name="proofreader_settings"
	      action="<?php echo esc_url(Proofreader_Admin::get_page_url('settings')); ?>"
	      method="POST">
		<div class="inside">
			<h3 class="title"><span><?php esc_html_e('Basic', 'proofreader-admin'); ?></span></h3>
			<table class="form-table">
				<tr>
					<th align="left" scope="row">
						<?php esc_html_e('Highlight Typos', 'proofreader-admin'); ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Highlight Typos', 'proofreader-admin'); ?></span>
							</legend>
							<label for="proofreader_highlight_typos_1">
								<input type="radio"
								       name="proofreader_highlight_typos"
								       id="proofreader_highlight_typos_1"
								       value="1" <?php checked('1', get_option('proofreader_highlight_typos', '1')); ?> /> <?php esc_html_e('Yes'); ?>
							</label>
							<label for="proofreader_highlight_typos_0">
								<input type="radio"
								       name="proofreader_highlight_typos"
								       id="proofreader_highlight_typos_0"
								       value="0" <?php checked('0', get_option('proofreader_highlight_typos')); ?> /> <?php esc_html_e('No'); ?>
							</label>

							<p class="description"><?php esc_html_e('Toggle whether typos should be highlighted on the site.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th align="left" scope="row">
						<?php esc_html_e('Notifications', 'proofreader-admin'); ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Notifications', 'proofreader-admin'); ?></span>
							</legend>
							<label for="proofreader_notifications_1">
								<input type="radio"
								       name="proofreader_notifications"
								       id="proofreader_notifications_1"
								       value="1" <?php checked('1', get_option('proofreader_notifications')); ?> /> <?php esc_html_e('Yes'); ?>
							</label>
							<label for="proofreader_notifications_0">
								<input type="radio"
								       name="proofreader_notifications"
								       id="proofreader_notifications_0"
								       value="0" <?php checked('0', get_option('proofreader_notifications', '0')); ?> /> <?php esc_html_e('No'); ?>
							</label>

							<p class="description"><?php esc_html_e('Toggle whether email notifications about typos should be sent to the editor.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th align="left" scope="row">
						<label for="proofreader_editor">
							<?php esc_html_e('Editor', 'proofreader-admin'); ?>
						</label>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Editor', 'proofreader-admin'); ?></span>
							</legend>
							<select name="proofreader_editor" id="proofreader_editor">
								<option><?php esc_html_e('Select a user'); ?></option>
								<?php foreach ($users as $user): ?>
									<option
										value="<?php echo $user['id']; ?>"
										<?php selected($user['id'], get_option('proofreader_editor')); ?>>
										<?php echo $user['name']; ?>
									</option>
								<?php endforeach; ?>
							</select>

							<p class="description"><?php esc_html_e('The user who will receive reports about typos on the site.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th align="left" scope="row">
						<?php esc_html_e('Handler', 'proofreader-admin'); ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Handler', 'proofreader-admin'); ?></span>
							</legend>
							<label for="proofreader_handler_keyboard">
								<input type="radio"
								       name="proofreader_handler"
								       id="proofreader_handler_keyboard"
								       value="keyboard" <?php checked('keyboard', get_option('proofreader_handler', 'keyboard')); ?> /> <?php esc_html_e('Keyboard (Ctrl+Enter)', 'proofreader-admin'); ?>
							</label>
							<br/>
							<label for="proofreader_handler_mouse">
								<input type="radio"
								       name="proofreader_handler"
								       id="proofreader_handler_mouse"
								       value="mouse" <?php checked('mouse', get_option('proofreader_handler')); ?> /> <?php esc_html_e('Mouse', 'proofreader-admin'); ?>
							</label>
							<br/>
							<label for="proofreader_handler_both">
								<input type="radio"
								       name="proofreader_handler"
								       id="proofreader_handler_both"
								       value="both" <?php checked('both', get_option('proofreader_handler')); ?> /> <?php esc_html_e('Both', 'proofreader-admin'); ?>
							</label>

							<p class="description"><?php esc_html_e('Select the handler which will be used by the Proofreader for report form. If selected \'Keyboard\' the Proofreader form could be opened by pressing keyboard shortcut Ctrl+Enter, if selected \'Mouse\' the floating button will appear after user has selected something on the page. Or you can use \'Both\' to support both handlers.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
			</table>

			<h3 class="title"><span><?php esc_html_e('Form', 'proofreader-admin'); ?></span></h3>
			<table class="form-table">
				<tr>
					<th align="left" scope="row">
						<?php esc_html_e('Comment', 'proofreader-admin'); ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Comment', 'proofreader-admin'); ?></span>
							</legend>
							<label for="proofreader_comment_1">
								<input type="radio"
								       name="proofreader_comment"
								       id="proofreader_comment_1"
								       value="1" <?php checked('1', get_option('proofreader_comment', '1')); ?> /> <?php esc_html_e('Show'); ?>
							</label>
							<label for="proofreader_comment_0">
								<input type="radio"
								       name="proofreader_comment"
								       id="proofreader_comment_0"
								       value="0" <?php checked('0', get_option('proofreader_comment')); ?> /> <?php esc_html_e('Hide'); ?>
							</label>

							<p class="description"><?php esc_html_e('You can set whether the user should specify the comment to his report about the found typo.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th align="left" scope="row">
						<?php esc_html_e('Dynamic Form Load', 'proofreader-admin'); ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Dynamic Form Load', 'proofreader-admin'); ?></span>
							</legend>
							<label for="proofreader_dynamic_form_load_1">
								<input type="radio"
								       name="proofreader_dynamic_form_load"
								       id="proofreader_dynamic_form_load_1"
								       value="1" <?php checked('1', get_option('proofreader_dynamic_form_load')); ?> /> <?php esc_html_e('Yes'); ?>
							</label>
							<label for="proofreader_dynamic_form_load_0">
								<input type="radio"
								       name="proofreader_dynamic_form_load"
								       id="proofreader_dynamic_form_load_0"
								       value="0" <?php checked('0', get_option('proofreader_dynamic_form_load', '0')); ?> /> <?php esc_html_e('No'); ?>
							</label>

							<p class="description"><?php esc_html_e('If set to \'Yes\' the Proofreader\'s form will be loaded dynamically using AJAX. Otherwise it will be included into the page\'s source.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th align="left" scope="row">
						<?php esc_html_e('CAPTCHA', 'proofreader-admin'); ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Captcha', 'proofreader-admin'); ?></span>
							</legend>
							<label for="proofreader_captcha_1">
								<input type="radio"
								       name="proofreader_captcha"
								       id="proofreader_captcha_1"
								       value="1" <?php checked('1', get_option('proofreader_captcha', '1')); ?> /> <?php esc_html_e('Yes'); ?>
							</label>
							<label for="proofreader_captcha_0">
								<input type="radio"
								       name="proofreader_captcha"
								       id="proofreader_captcha_0"
								       value="0" <?php checked('0', get_option('proofreader_captcha')); ?> /> <?php esc_html_e('No'); ?>
							</label>

							<p class="description"><?php esc_html_e('Use reCAPTCHA to protect the Proofreader\'s form from spam bots. IMPORTANT: You MUST provide an reCAPTCHA API keys if you turn this on.', 'proofreader-admin') ?></p>
						</fieldset>
					</td>
				</tr>
			</table>

			<h3 class="title"><span><?php esc_html_e('reCAPTCHA Keys', 'proofreader-admin'); ?></span></h3>
			<table class="form-table">
				<tr>
					<th align="left" scope="row">
						<label for="proofreader_recaptcha_site_key">
							<?php esc_html_e('Site key', 'proofreader-admin'); ?>
						</label>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Site key', 'proofreader-admin'); ?></span>
							</legend>
							<input type="text"
							       id="proofreader_recaptcha_site_key"
							       name="proofreader_recaptcha_site_key"
							       maxlength="100"
							       value="<?php echo esc_attr(get_option('proofreader_recaptcha_site_key', '')); ?>">

							<p class="description"><?php echo __('Used for displaying the CAPTCHA. Grab it <a href="https://www.google.com/recaptcha/admin" target="_blank">Here</a>.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th align="left" scope="row">
						<label for="proofreader_recaptcha_secret_key">
							<?php esc_html_e('Secret key', 'proofreader-admin'); ?>
						</label>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Secret key', 'proofreader-admin'); ?></span>
							</legend>
							<input type="text"
							       id="proofreader_recaptcha_secret_key"
							       name="proofreader_recaptcha_secret_key"
							       maxlength="100"
							       value="<?php echo esc_attr(get_option('proofreader_recaptcha_secret_key', '')); ?>">

							<p class="description"><?php echo __('Used for communication between your site and Google. Grab it <a href="https://www.google.com/recaptcha/admin" target="_blank">Here</a>.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
			</table>

			<h3 class="title">
				<span><?php esc_html_e('Advanced', 'proofreader-admin'); ?></span>
			</h3>
			<table class="form-table">
				<tr>
					<th align="left" scope="row">
						<label for="proofreader_selection_limit">
							<?php esc_html_e('Selection Limit', 'proofreader-admin'); ?>
						</label>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Selection Limit', 'proofreader-admin'); ?></span>
							</legend>
							<input type="number"
							       id="proofreader_selection_limit"
							       name="proofreader_selection_limit"
							       maxlength="10"
							       value="<?php echo esc_attr(get_option('proofreader_selection_limit', '100')); ?>">

							<p class="description"><?php esc_html_e('Sets the maximum text length (number of characters) to be marked as typo.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
				<tr>
					<th align="left" scope="row">
						<?php esc_html_e('Disable CSS', 'proofreader-admin'); ?>
					</th>
					<td>
						<fieldset>
							<legend class="screen-reader-text">
								<span><?php esc_html_e('Disable CSS', 'proofreader-admin'); ?></span>
							</legend>
							<label for="proofreader_disable_css_1">
								<input type="radio"
								       name="proofreader_disable_css"
								       id="proofreader_disable_css_1"
								       value="1" <?php checked('1', get_option('proofreader_disable_css')); ?> /> <?php esc_html_e('Yes'); ?>
							</label>
							<label for="proofreader_disable_css_0">
								<input type="radio"
								       name="proofreader_disable_css"
								       id="proofreader_disable_css_0"
								       value="0" <?php checked('0', get_option('proofreader_disable_css', '0')); ?> /> <?php esc_html_e('No'); ?>
							</label>

							<p class="description"><?php esc_html_e('Disallow usage of built-in CSS files and thus lower the number of used CSS files. Use this feature if you have already included the Proofreader\'s styles in the main template\'s CSS.', 'proofreader-admin'); ?></p>
						</fieldset>
					</td>
				</tr>
			</table>
		</div>
		<div id="major-publishing-actions">
			<div id="publishing-action">
				<?php wp_nonce_field('proofreader_save_settings_nonce', '_wpnonce'); ?>
				<input type="hidden" name="action" value="save-settings">
				<input type="submit" name="submit" id="submit" class="button button-primary"
				       value="<?php esc_attr_e('Save Changes'); ?>">
			</div>
			<div class="clear"></div>
		</div>
	</form>
</div>
