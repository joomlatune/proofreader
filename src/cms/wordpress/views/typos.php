<?php defined('ABSPATH') or die('No script kiddies please!'); ?>
<div class="wrap">

	<h2><?php esc_html_e('Typos', 'proofreader-admin'); ?></h2>

	<form name="proofreader_typos" id="proofreader_typos"
	      action="<?php echo esc_url(Proofreader_Admin::get_page_url()); ?>" method="GET">
		<input type="hidden" name="action" value="typos">
		<input type="hidden" name="page" value="proofreader"/>
		<?php /** @var Proofreader_List_Table $list_table */ ?>
		<?php $list_table->search_box(__('Search'), 'typos_search'); ?>
		<?php $list_table->display(); ?>
		<?php wp_nonce_field('proofreader_typos_actions_nonce', '_wpnonce'); ?>
	</form>
</div>